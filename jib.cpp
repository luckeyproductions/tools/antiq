/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "jib.h"


Jib::Jib(Context* context) : Camera(context),
    pivot_{ Vector3::ZERO },
    mouseRay_{},
    zoomDirection_{ Vector3::FORWARD }
{
    SetTemporary(true);
}

void Jib::move(Vector3 movement, MoveType type, bool scroll)
{
    if (movement == Vector3::ZERO)
        return;

    const bool ortho{ IsOrthographic() };
    const float pivotDistance{ pivot_.DistanceToPlane(node_->GetPosition(), node_->GetDirection()) };
    const float panSpeed{ 1.25f + (ortho ? (GetOrthoSize() * 1.3f) : (pivotDistance * GetFov() * 0.028f)) };
    const float rotationSpeed{ 235.0f };

    switch (type)
    {
    case MT_FOV:
    {
        if (!ortho)
            SetFov(Clamp(GetFov() + 23.0f * movement.y_, 5.0f, 120.0f));
        else
            node_->Translate(Vector3::BACK * movement.y_ * panSpeed);

    } break;
    case MT_PAN:
    {
        movement *= scroll ? 10.0f : panSpeed;

        if (!ortho) {

            node_->Translate(node_->GetRight()                          * -movement.x_ +
                             node_->GetUp()                             *  movement.y_ +
                   (scroll ? node_->GetDirection() : zoomDirection_)    *  movement.z_, TS_WORLD);

        } /*else {

           mouseRay_ camera_->SetOrthoSize(Max(1.0f, camera_->GetOrthoSize() + movement.z_ * -1.3f));
            Vector3 lockVector{ cursor->GetLockVector() };

            if (lockVector.Length() != 1.0f) { ///Needs more cases or generalisation

                if (lockVector.y_ == 0.0f)
                    movement.y_ /= Abs(node_->GetDirection().DotProduct(Vector3::UP) * M_SQRT2);
//                else
//                    movement.x_ /= Abs(node_->GetDirection().DotProduct(Vector3::FORWARD) * M_SQRT2);
            }

            node_->Translate((lockVector * node_->GetRight()).Normalized() * -movement.x_
                           + (lockVector * node_->GetUp()).Normalized()    *  movement.y_, TS_WORLD);

        }*/
    } break;
    case MT_ROTATE:
    {
        const float pitchDelta{ clampPitch(movement.y_ * rotationSpeed) };
        const Quaternion rotation{ Quaternion{ movement.x_ * rotationSpeed, Vector3::UP } *
                                   Quaternion{ pitchDelta, node_->GetRight() } };

        node_->RotateAround(pivot_, rotation, TS_WORLD);

    } break;
    case MT_TURN:
    {
        const float pitchDelta{ clampPitch(movement.y_ * rotationSpeed * 0.333f) };

        node_->Rotate(Quaternion{ movement.x_ * rotationSpeed * 0.666f, Vector3::UP } *
                      Quaternion{ pitchDelta, node_->GetRight() }, TS_WORLD);
    } break;
    default: break;
    }
}

float Jib::clampPitch(float pitchDelta)
{
    const float resultingPitch{ node_->GetRotation().PitchAngle() + pitchDelta };

    if (resultingPitch > PITCH_MAX)
        pitchDelta -= resultingPitch - PITCH_MAX;
    else if (resultingPitch < PITCH_MIN)
        pitchDelta -= resultingPitch - PITCH_MIN;

    return pitchDelta;
}

void Jib::updatePivot()
{
    const Octree* const octree{ GetScene()->GetComponent<Octree>() };

    if (!octree)
        return;

    PODVector<RayQueryResult> result{};
    RayOctreeQuery query{ result, mouseRay_, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY };

    octree->RaycastSingle(query);

    if (result.IsEmpty())
    {
        const Ray cameraRay{ mouseRay_.origin_, centeredToCamera(mouseRay_.direction_, true).Normalized() };
        RayOctreeQuery query{ result, cameraRay, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY };

        octree->RaycastSingle(query);
    }

    if (!result.IsEmpty())
    {
        pivot_ = result.Front().position_;
    }
    else
    {
        const float distanceToPivot{ node_->GetWorldPosition().DistanceToPoint(pivot_) };
        pivot_ = node_->GetWorldPosition() + mouseRay_.direction_ * distanceToPivot;
    }

    pivot_ = centeredToCamera(pivot_);
    zoomDirection_ = mouseRay_.direction_;
}

void Jib::updateMouseRay(const Vector2& mousePos)
{
    mouseRay_ = GetScreenRay(mousePos);
}

Vector3 Jib::centeredToCamera(const Vector3& vector, bool direction)
{
    if (direction)
        return vector.ProjectOntoPlane(Vector3::ZERO, node_->GetRight());
    else
        return vector.ProjectOntoPlane(node_->GetWorldPosition(), node_->GetRight());
}
