![](images/antiq_big.svg)

# Antiq

_The green pill_

A Pirmitide, using Qt and Dry.

[![](screenshots/one.png)](screenshots/one.png)

## Hololith

The hololith is capable to three interpirmitations. Each with its own Lum overlay.

+ Pixel: Qt
+ 2D: Qt
+ 3D: Dry

## Lum

### Specter

Plasme display equipped for lezer surgery and rooster sparking.

### Armer

Curve and surface editor

### Snail

Mes validator; picky about the glyphs it eats and excretes a complete mes.

## Keys

[]() | Action
---|---
Ctrl+R| Red mode
Ctrl+G| Green mode
Ctrl+B| Blue mode
Ctrl+/| Alpha mode
Ctrl+A| Toggle Armer
Ctrl+S| Toggle Specter
Ctrl+E| Plain text export
Ctrl+1| Pixelith
Ctrl+2| 2Dlith
Ctrl+3| 3Dlith
