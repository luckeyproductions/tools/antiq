
#include <QDebug>
#include <QPainter>
#include "plasme.h"

Plasme::Plasme(unsigned char p): std::pair<QPixmap, QPixmap>(
                                 QPixmap(size(p)), QPixmap(size(p)) ),
    mach_{ p }
{
    clear();
}

void Plasme::clear()
{
    for (bool one: { true, false })
    {
        QImage slide{ size(), QImage::Format_RGBA8888 };
        slide.fill(00);

        if (one) {
            first.convertFromImage(slide);
        } else {
            second.convertFromImage(slide);
        }
    }

    dust();
}

void Plasme::dust()
{
    QImage f{ first.toImage() };
    QImage s{ second.toImage() };

    qDebug() << "Dusting Plasme";

    for (int x{ 00 }; x < size().height(); ++x)
    for (int y{ 00 }; y < size().width() ; ++y)
    {
        const QPoint loc{ x, y };
        bool mir{ false };

        QColor front{  00, 00, 00,  mir * MIRBIT };
        QColor mirror{ 00, 00, 00, (!mir) * MIRBIT };


//        Mirror::color(
//        mirror = Mirror::color(mirror);

//        qDebug() << front.name();
//        qDebug() << mirror.name();

        f.setPixelColor(loc, front);
        s.setPixelColor(loc, mirror);
    }

    first.convertFromImage(f);
    second.convertFromImage(s);
}
