#ifndef HOLOLITH_H
#define HOLOLITH_H

#include <QLabel>
#include <QResizeEvent>

#include "dry.h"
#include <QStackedWidget>

class PixelView;
class View2D;
class View3D;

class PreView: public QWidget
{
    Q_OBJECT
public:
    explicit PreView(Context* context, QWidget* parent = nullptr);

private:
    PixelView* pixelView_;
    View2D* view2d_;
    View3D* view3d_;
};

class Hololith : public QStackedWidget
{
    Q_OBJECT
public:
    explicit Hololith(Context* context, QWidget* parent = nullptr);

private:
    PreView* off_;
    PixelView* pixelView_;
    View2D* view2d_;
    View3D* view3d_;
};

#endif // HOLOLITH_H
