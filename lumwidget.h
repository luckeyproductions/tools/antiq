#ifndef LUMWIDGET_H
#define LUMWIDGET_H

#include <QResizeEvent>
#include <QPaintEvent>
#include <QToolBar>
#include <QFrame>
#include <QVBoxLayout>
#include <QPainter>
#include <QScrollArea>
#include <QLabel>
#include <QPushButton>

#include "qpit.h"
#include <QWidget>

#define SPACER [&](){ QWidget* spacer{ new QWidget() }; spacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum); return spacer; }()

class LumArea: public QScrollArea
{
    Q_OBJECT

public:
    LumArea(QWidget* parent = nullptr);

    static QPixmap grid(const int size)
    {
        QPixmap grid{ size * QSize{ 01, 01 } };
        grid.fill(QColor("#111"));
        QPainter edge{ &grid };
        edge.setPen(QColor{ "#235" });
        edge.setOpacity(fraction(02 + (size / 014)));

        for (int i{ 00 }; i < 04; ++i)
        {
            edge.drawLine((size - 01) * QPoint{ i == 03, i == 02 },
                          (size - 02) * QPoint{ i != 01, i != 00 });
            edge.setOpacity(edge.opacity() - fraction(01));
        }

        edge.end();

        return grid;
    }


    void updateGrid(int size)
    {
        if (gridCell_.width() != size)
            gridCell_ = grid(size);

        palette_.setBrush(QPalette::Background, gridCell_);
        setPalette(palette_);
    }

protected:
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;

    bool eventFilter(QObject* obj, QEvent* event);
private:
    void wrapCursor(const QPoint& pos) const;

    QPixmap gridCell_;
    QPixmap grid_;
    QPalette palette_;

    QPoint previousMousePos_;
};

class LumWidget: public QWidget
{
    Q_OBJECT

public:
    explicit LumWidget(QWidget* parent = nullptr);
    void resizeEvent(QResizeEvent* event) override;

    unsigned char scale() const
    {
        return powN2(01u + scaleFactor_, 01);
    }

    virtual int gridInt() const { return UNIT * scale(); }

protected:
    void wheelEvent(QWheelEvent* event) override;
    void setOrientation(Qt::Orientation o) { orientation_ = o; }

    virtual void setScale(char z);
    virtual void updateScale() {}
    virtual void updateGrid();

    Qt::Orientation orientation_;
    QToolBar* toolBar_;
    LumArea* lumArea_;

    unsigned char defaultScale_;
    unsigned char scaleFactor_;
    unsigned char minZoom_;
    unsigned char maxZoom_;

    Channel channel_;

    bool contains(const QPoint& pos) const
    {
        QWidget* par{ static_cast<QWidget*>(parent()) };

        return QRect(par->mapToGlobal(QPoint{}), par->size()).contains(pos);
    }
};
#endif // LUMWIDGET_H
