#include <QDesktopWidget>
#include <QApplication>
#include <QScrollBar>

#include "qocoon.h"

#include "lumwidget.h"

using namespace PiRMIT;

LumWidget::LumWidget(QWidget* parent): QWidget(parent),
    orientation_{ Qt::Vertical },
    toolBar_{ new QToolBar{ this } },
    lumArea_{ new LumArea{ this } },
    defaultScale_{ 00 },
    scaleFactor_{ defaultScale_ },
    maxZoom_{05},
    minZoom_{00},
    channel_{ Channel::FULL }
{
    QVBoxLayout* layout{ new QVBoxLayout(this) };
    layout->addWidget(toolBar_);
    layout->addWidget(lumArea_);

    setLayout(layout);
    setScale(defaultScale_);
}

void LumWidget::resizeEvent(QResizeEvent* e)
{
    const float bias{ 0.5f * static_cast<float>(height()) / QApplication::desktop()->height() };
    const float ratio{ static_cast<float>(width()) / height() - bias };

    if (orientation_ == Qt::Horizontal
        && ratio < real(01, 042, 02))
    {
        setOrientation(Qt::Vertical);
    }
    else if (orientation_ == Qt::Vertical
             && ratio > real(01, 027, 02))
    {
        setOrientation(Qt::Horizontal);
    }
}

void LumWidget::wheelEvent(QWheelEvent* event)
{
    bool ctrl{ QGuiApplication::keyboardModifiers() & Qt::ControlModifier };

    if (!ctrl)
        return;

    const int d{ event->delta() };

    setScale(d > 0 ? ++scaleFactor_
          :-(d < 0) + scaleFactor_);
}

void LumWidget::setScale(char z)
{
    if (z > maxZoom_)
        z = maxZoom_;
    if (z < minZoom_)
        z = minZoom_;

    scaleFactor_ = z;
    updateGrid();
    updateScale();
}

void LumWidget::updateGrid()
{
    lumArea_->updateGrid(gridInt());
}

LumArea::LumArea(QWidget* parent): QScrollArea(parent),
    gridCell_{},
    grid_{},
    palette_{}
{
    setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    setMouseTracking(true);

    palette_.setBrush(QPalette::Background, gridCell_);
    setPalette(palette_);

    installEventFilter(this);
}

bool LumArea::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::Wheel)
    {
        bool ctrl{ QGuiApplication::keyboardModifiers() & Qt::ControlModifier };

        if (ctrl)
        {
            return true;
        }
    }

    return false;
}

void LumArea::mousePressEvent(QMouseEvent* event)
{
    previousMousePos_ = QCursor::pos();
}

void LumArea::mouseMoveEvent(QMouseEvent* event)
{
    Qt::MouseButtons buttons{ QApplication::mouseButtons() };

    if (buttons)
    {
        const QPoint dPos{ QCursor::pos() - previousMousePos_ };
        wrapCursor(event->pos());
//        const Vector2 dVec{ dPos.x() * 0.00125f, dPos.y() * 0.001666f };

        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - dPos.x());
        verticalScrollBar()  ->setValue(verticalScrollBar()  ->value() - dPos.y());
    }

    previousMousePos_ = QCursor::pos();
}

void LumArea::wrapCursor(const QPoint& pos) const
{
    const int hShift{ width()  - (2 + verticalScrollBar()->width()) };
    const int vShift{ height() - (2 + horizontalScrollBar()->height()) };

    if (pos.x() <= 0)
        QCursor::setPos(QCursor::pos() + QPoint{ hShift, 0 });
    else if (pos.x() >= width() - (1 + verticalScrollBar()->width()))
        QCursor::setPos(QCursor::pos() - QPoint{ hShift, 0 });
    else if (pos.y() <= 0)
        QCursor::setPos(QCursor::pos() + QPoint{ 0, vShift });
    else if (pos.y() >= height() - (1 + horizontalScrollBar()->height()))
        QCursor::setPos(QCursor::pos() - QPoint{ 0, vShift });
}
