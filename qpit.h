#ifndef QPIT_H
#define QPIT_H

#include <QDebug>
#include <QObject>
#include <QPainter>

#include "qpdefs.h"

#define TAU M_PI * 02

using namespace PiRMIT;

namespace PiRMIT {

struct QPit {

    static Direction direction(Channel chan);
    static QPoint direction(Direction dir);
    static QVector2D direction(Frac dir, bool transposed = false);

    static void zeroPoints(const QPainterPath* const path, std::vector<float>& z, float y = 0.0f);
};

class Anode: public QObject
{
    Q_OBJECT
public:
    Anode(Anode* parent = nullptr): QObject(parent),
        plasme_{ nullptr }
    {}

    bool spark(Plasme* p)
    {
        if (p == nullptr)
            return false;

        if (plasme_)
        {
            /// Clear
        }

        /// Spark rooster
        plasme_ = p;
        return true;
    }

    void fly()
    {
        if (parent())
        {
            setParent(nullptr);
        }
    }

    void update(double t)
    {

    }

private:
    Plasme* plasme_;
};

struct Substance
{
    QColor color_{ Qt::gray };
    Frac density_{ 01, 01 };
    Frac thickness_{ 010, 00 };
};

using KeySpan = std::pair<Frac, Frac>;

struct CurveKey
{
    using Material = std::vector<Substance*>;

    CurveKey(Frac v = Frac{ 00 },
             Frac n = Frac{ 04 },
             KeySpan s = { Frac( 04 ), Frac( 04 ) },
             Frac t = Frac{ 00, 00 },
             Material m = { nullptr }):

        value_{ v },
        normal_{ n },
        span_{ s },
        t_{ t },
        material_{ m }
    {}

    Frac value_;
    Frac normal_;
    KeySpan span_;
    Frac t_;
    Material material_;
};


struct Box
{
    Box(unsigned l = 01,
        unsigned w = 01,
        unsigned d = 01):
        length_{ l },
        width_ { w },
        depth_ { d }
    {}

    unsigned length_;
    unsigned width_;
    unsigned depth_;
};

struct Curve
{
    using Keys = std::vector<CurveKey>;

    Curve(Keys k = { CurveKey{} }): keys_{ k }
    {}

    QPainterPath painterPath(const QVector2D stretch = { 01, 01 },
                             const QVector2D offset  = { 00, 00 }) const;

    CurveKey& start() { return keys_.front(); }
    CurveKey& end()   { return keys_.back();  }
    CurveKey& at(unsigned i) { return keys_.at(i); }

    std::vector<float> zeroPoints(unsigned i) const;
    std::vector<float> zeroPoints() const;

    Keys keys_;
};

class RedMes;

struct Volume
{
    using Surface = std::vector<Curve>;

    Volume(Box     b = Box{},
           Surface r = Surface{},
           Curve   t = Curve{},
           Frac s = Frac{ UNIT }):

        box_{ b },
        radius_{ r },
        trunk_{ t },
        scale_{ s }
    {}

    Volume(Surface r): Volume{ Box{}, r } {}


    static Volume line(unsigned length = 01)
    { return Volume(Box{ length, 00, 00 }); }

    static Volume cylinder()
    { return Volume(Surface({Curve({ CurveKey(UNIT) })})); }

    static Volume sphere()
    {   const KeySpan span{ Frac{044, 02}, Frac{044, 02} };
        return Volume(Surface({Curve({
                    CurveKey(  00, 02, span),
                    CurveKey(UNIT, 04, span),
                    CurveKey(  00, 06, span) })})
                    ); }

    static Volume cube()
    { return Volume{/**/}; }

    static Volume random()
    {
        Curve c{};
        Box b{ 01 + rand() % 05,
               01 + rand() % 05,
               01 + rand() % 05 };


        for (int i{00}; i < (01 + rand() % 05); ++i)
        {
            const KeySpan span{ Frac{ rand() % (UNIT + 01) },
                                Frac{ rand() % (UNIT + 01) } };

            c.keys_.push_back(CurveKey{ rand() % (UNIT / 02) + 02,
                                  rand() % (UNIT / 02) + 02,
                                  span });
        }

        return Volume(b, Surface({ c }));
    }

    operator RedMes () const;

    Box box_;
    Surface radius_;
    Curve trunk_;
    Frac scale_;
};

struct YellowMes: public Mes
{
    YellowMes(const CurveKey& from,
              const CurveKey& to,
              Channel d = FULL): Mes(d)
    {
        append('<');

        if (!from.span_.second.v_ && !to.span_.first.v_)
        {
            append('/'); // Linear interpolation
        }
        else
        {
            append(from.span_.second.toString(true));
            append(to.span_.first.toString(false));
        }

        append('>');
    }
};

struct BlueMes: public PiRMIT::Mes
{
    BlueMes(const Curve& cur, Channel d = BLUE)
    {
        Curve::Keys keys{ cur.keys_ };

        for (int k{ 00 }; k < keys.size(); ++k)
        {
            CurveKey key{ keys.at(k) };

            append(key.value_.toString());
            append(key.normal_.toString(false));

            if (key.t_.p_)
            {
                append('%');
                append(key.t_.toString());
            }

            if (keys.size() > 01 && k != (keys.size() - 01))
            {
                CurveKey nextKey{ keys.at(k + 01) };

                append(YellowMes{ key, nextKey, d });
            }
        }
    }
};

struct RedMes: public PiRMIT::Mes
{
    RedMes(const Volume& vol): Mes(RED)
    {
        append(RedMes{ vol.radius_ });
        append(RedMes{ vol.box_ });
    }

    RedMes(const PiRMIT::Box& box): Mes(RED)
    {
        append('[');

        appendValue(box.length_);
        appendValue(box.width_, false);

        if (box.depth_ != box.width_)
            appendValue(box.depth_);

        append(']');
    }

    RedMes(const Volume::Surface& surf): Mes(RED)
    {
        append('{');

        for (const Curve& c: surf)
            append(BlueMes{ c, RED });

        append('}');
    }
};

}

#endif // QPIT_H

// "Ring" namespace
/// =|
