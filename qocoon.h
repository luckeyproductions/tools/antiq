/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QOCOON_H
#define QOCOON_H

#include <QMap>
#include <QMainWindow>
#include <QUndoView>

#include "qpit.h"
#include "drydockwidget.h"
#include "dry.h"

enum ViewMode{ ViewOFF, ViewONE, ViewTWO, ViewTHREE, ViewALL };
static const Vector<ViewMode> MODES { ViewONE, ViewTWO, ViewTHREE };

class Project;
class NoProjectWidget;
class Hololith;
class Specter;
class Flux;
class Armer;
class Snail;

class Qocoon : public QMainWindow, public Object
{
    Q_OBJECT
    DRY_OBJECT(Qocoon, Object)

public:
    explicit Qocoon(Context* context);
//    explicit Qocoon(Context* context, Material* material);
    ~Qocoon();

    bool loadProject(QString filename);

    QDockWidget* createDockWidget(QWidget* w, Qt::DockWidgetArea area)
    {
        QDockWidget* dockWidget{ new QDockWidget(w->objectName(), this) };
        dockWidget->setWidget(w);
        dockWidget->setObjectName(w->objectName() + "Dock");
        w->setParent(dockWidget);

        addDockWidget(area, dockWidget);
        return dockWidget;
    }

    static Project* project() { return project_; }
    static String projectLocation();
    static bool inResourceFolder(const String& fileName);
    static String trimmedResourceName(const String& fileName);
    static String containingFolder(const String& path);
    static String absoluteResourceFolder(const String& path);
    static String locateResourceRoot(String resourcePath);

    static Flux*    flux_;
    static Specter* specter_;
    static Armer*   armer_;
    static Snail*   snail_;

signals:
    void resourceFoldersChanged();
    void recentProjectsChanged();
    void currentProjectChanged();

public slots:
    void openRecent() { openProject(sender()->objectName()); }

protected:
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void newPlasme();
    void openProject(QString filename = "");
    bool saveProject();
    void addResourceFolder();
    void updateRecentProjectMenu();
    void updateAvailableActions();

    void toggleFullView(bool toggled);
    void about();
    void openUrl();
    void changeMode(bool toggled);
    void undo();
    void redo();

    void closeProject();
private:
    QMenu* createMenuBar();
    void createToolBar();
    void loadSettings();
    void updateRecentProjects();
    QString modeToString(ViewMode mode);
    void setMode(ViewMode mode);

    static SharedPtr<Project> project_;

    bool singleWidget_;
    ViewMode mode_;
    QMap<ViewMode, QAction*> modeActions_;
    std::vector<QAction*> projectDependentActions_;
    QToolBar* mainToolBar_;
    Hololith* hololith_;

    QUndoView* undoView_;

    QMenu* recentMenu_;
    std::vector<QDockWidget*> hiddenDocks_;
    QAction* fullViewAction_;
};

#endif // QOCOON_H
