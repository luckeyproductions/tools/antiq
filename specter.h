#ifndef SPECTER_H
#define SPECTER_H

#include <QLabel>
#include <QLayout>
#include <bitset>

#include "lumwidget.h"

class Plasme;
class Laser;

class Sheet: public QWidget
{
    friend class Specter;

    Sheet(QWidget* parent);

    void project(QPixmap* front, QPixmap* back);
    void setSize(QSize size);

    QLabel* front_;
    QPalette palette_;
};

class Specter: public LumWidget
{
    Q_OBJECT

public:
    explicit Specter(QWidget* parent = nullptr);

    bool reveal(Plasme* p);
    void newSheet() {}
    Plasme* plasme() const { return visible_; }

    unsigned char charAt(const QPoint loc, Channel chan, bool front = true);

signals:
    void revealed(Plasme* p);

public slots:
    void putMes(Mes mes, bool mir = false);
    void putChar(QPoint cell, unsigned char c, Channel d, bool mir);

protected:
    QSize displaySize() { return visible_ ? visible_->size() * scale()
                                          : QSize{ 00, 00 }; }
    void updateScale() override;
    void resizeEvent(QResizeEvent* e) override;

protected slots:
    void zoomIn();
    void zoomOut();

private:
    void sieve(Plasme* p);
    Tux riid(Peer& p);
    void putMes(QPoint cell, Direction d, Mes mes, bool mir = false);

    Laser* laserBar_;

    Sheet* sheet_;
    Plasme* visible_;
    QPixmap firstSide_;
    QPixmap mirSide_;
    QPixmap ghost_;

    std::bitset<4> filter_;
};

#endif // SPECTER_H
