/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef JIB_H
#define JIB_H

#include "dry.h"

#define PITCH_MAX 90.0f
#define PITCH_MIN -90.0f

enum MoveType { MT_ROTATE, MT_PAN, MT_FOV, MT_TURN };

class Jib : public Camera
{
    DRY_OBJECT(Jib, Camera)
public:
    Jib(Context* context);

    void move(Vector3 movement, MoveType type, bool scroll = false);
    void updateMouseRay(const Vector2& mousePos);
    void updatePivot();
    void setZoomDirection(Vector3 direction) { zoomDirection_ = direction; }
    Vector3 zoomDirection() const { return zoomDirection_; }
    Vector3 direction() const { return node_->GetDirection(); }

private:
    float clampPitch(float pitchDelta);
    Vector3 centeredToCamera(const Vector3& vector, bool direction = false);

    Vector3 pivot_;
    Ray     mouseRay_;
    Vector3 zoomDirection_;
};

#endif // JIB_H
