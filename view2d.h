#ifndef VIEW2D_H
#define VIEW2D_H

#include "holoview.h"


class View2D: public HoloView
{
    Q_OBJECT

public:
    View2D(QWidget* parent = nullptr);
};

#endif // VIEW2D_H
