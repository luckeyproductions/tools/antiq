/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROJECT_H
#define PROJECT_H

#include "dry.h"


class Project : public Serializable
{
    DRY_OBJECT(Project, Serializable);

public:
    Project(Context* context);

    void OnSetAttribute(const AttributeInfo& attr, const Variant& src) override;
    bool Save() const;
    bool LoadXML(const XMLElement& source) override;
    bool SaveXML(XMLElement& dest) const override;
    void ApplyAttributes()  override;

    bool setLocation(const String& path);
    bool setName(const String& name);
    bool addResourceFolder(const String& path, bool emitChange = true);
    bool removeResourceFolder(const String& path, bool emitChange = true);

    String location() const { return location_; }
    String name() const { return name_; }
    const StringVector& resourceFolders() const { return resourceFolders_; }
    String absoluteResourceFolder(const String& path) const;
    String localizedPath(const String& path) const;
    bool inResourceFolder(const String& fileName);
    bool inProjectFolder(const String& path);

    void remove();

private:
    String location_;
    String name_;
    StringVector resourceFolders_;
};

#endif // PROJECT_H
