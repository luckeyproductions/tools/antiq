/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef VIEW3DWIDGET_H
#define VIEW3DWIDGET_H

#include "jib.h"
#include "filewidget.h"

class View3D: public FileWidget
{
    Q_OBJECT
    DRY_OBJECT(View3D, FileWidget)

public:
    explicit View3D(Context* context, QWidget* parent = nullptr);

    void setScene(Scene* scene);
    void setContinuousUpdate(bool enabled) { continuousUpdate_ = enabled; }
    bool continuousUpdate() const { return continuousUpdate_; }
    Camera* camera() const { return jib_; }
    Scene* scene() const { return scene_; }

public slots:
    virtual void updateView();
    void toggleContinuousUpdate() { setContinuousUpdate(!continuousUpdate()); }

protected:
    virtual void updateViewport();

    void resizeEvent(QResizeEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

    void mouseMoveEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void wrapCursor(const QPoint& pos);

    Scene* scene_;
    Camera* activeCamera_;
    Jib* jib_;
    SharedPtr<Texture2D> renderTexture_;
    QPoint previousMousePos_;

private:
    void createRenderTexture();
    void updatePixmap();
    void updateView(StringHash eventType, VariantMap& eventData);
    void paintView();
    void paintView(StringHash eventType, VariantMap& eventData);

    SharedPtr<Image> image_;
    QPixmap pixmap_;
    bool continuousUpdate_;
};

#endif // VIEW3DWIDGET_H
