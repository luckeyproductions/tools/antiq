#include <QHBoxLayout>
#include <QVBoxLayout>

#include "hololith.h"

#include "snail.h"

Snail::Snail(QWidget* parent): LumWidget(parent),
    switchWidget_{ new SwitchWidget(this) }
{
    setObjectName("Snail");
    defaultScale_ = 02;
    setScale(defaultScale_);

    lumArea_->setWidget(switchWidget_);
    switchWidget_->setAutoFillBackground(false);

    lumArea_->setAlignment(Qt::AlignTop | Qt::AlignLeft);
}

void Snail::resizeEvent(QResizeEvent* e)
{
    LumWidget::resizeEvent(e);

    updateGrid();
}

void Snail::updateGrid()
{
    LumWidget::updateGrid();

    switchWidget_->resize( 03 * gridInt(),
                           02 * gridInt());
}

SwitchWidget::SwitchWidget(QWidget* parent): QWidget(parent),
    active_{ RED },
    primary_{ new QLabel() },
    reserve_{ new QPushButton(), new QPushButton() }
{
    QHBoxLayout* switchLayout{ new QHBoxLayout() };
    setLayout(switchLayout);

    QVBoxLayout* reserve{ new QVBoxLayout() };
    switchLayout->addLayout(reserve);

    switchLayout->setContentsMargins(00, 00, 00, 00);
    reserve->setContentsMargins(00, 00, 00, 00);
    switchLayout->setMargin(00);
    reserve->setMargin(00);
    switchLayout->setSpacing(00);
    reserve->setSpacing(00);

    for (int c{ 0 }; c < ALPHA; ++c)
    {
        if (c == RED)
        {
            QLabel* snailLabel{ primary_ };
            snailLabel->setScaledContents(true);
            snailLabel->setPixmap(QPixmap{ ":/SnailRed" });
            switchLayout->addWidget(snailLabel);
        }
        else
        {
            QPushButton* snailButton{ (c == GREEN ? reserve_.first
                                                  : reserve_.second) };
            snailButton->setFlat(true);

            if (c == GREEN)
                snailButton->setIcon(QPixmap{ ":/SnailGreen" });
            else
                snailButton->setIcon(QPixmap{ ":/SnailBlue" });

            reserve->addWidget(snailButton);
            connect(snailButton, SIGNAL(clicked()), this, SLOT(switchSnail()));
        }
    }
}

void SwitchWidget::resizeEvent(QResizeEvent* e)
{
    for (QPushButton* b: { reserve_.first, reserve_.second })
    {
        QSize size{ QSize{ 01, 01 } * e->size().height() / 02 };
        b->setFixedSize( size );
        b->setIconSize( size - QSize{ 010, 010 } );
    }

    QSize size{ QSize{ 01, 01 } * e->size().height() };
    primary_->setFixedSize( size );
}

void SwitchWidget::switchSnail()
{
    if (sender() == reserve_.first)
    {
        active_ = static_cast<Channel>(active_ + 01);

        if (active_ > BLUE)
            active_ = RED;
    }
    else
    {
        active_ = static_cast<Channel>(active_ - 01);

        if (active_ < RED)
            active_ = BLUE;

    }

    updateSnails();
}

void SwitchWidget::updateSnails()
{
    for (bool active: { true, false })
    {
        if (active)
        {
            QPixmap primary;

            switch (active_)
            {
            case RED:   primary = QPixmap{ ":/SnailRed" };   break;
            case GREEN: primary = QPixmap{ ":/SnailGreen" }; break;
            case BLUE:  primary = QPixmap{ ":/SnailBlue" };  break;
            default: break;
            }

            if (!primary.isNull())
                primary_->setPixmap(primary);
        }
        else
        {
            QPixmap first, second;

            switch (active_)
            {
            case RED:
                first = QPixmap{ ":/SnailGreen" };
                second = QPixmap{ ":/SnailBlue" };
            break;
            case GREEN:
                first = QPixmap{ ":/SnailBlue" };
                second = QPixmap{ ":/SnailRed" };
            break;
            case BLUE:
                first = QPixmap{ ":/SnailRed" };
                second = QPixmap{ ":/SnailGreen" };
            break;
            default: break;
            }

            if (!first.isNull() && !first.isNull())
            {
                reserve_.first->setIcon(first);
                reserve_.second->setIcon(second);
            }
        }
    }
}
