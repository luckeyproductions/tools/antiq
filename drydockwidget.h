/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DRYDOCKWIDGET_H
#define DRYDOCKWIDGET_H

#include "drywidget.h"

#include <QDockWidget>
#include <QProxyStyle>
#include <QStyleOption>
/*
class DryDockStyle: public QProxyStyle // For icon in titlebar, does not include tabs
{
    Q_OBJECT

public:
    DryDockStyle(const QIcon& icon,  QStyle* style = 0): QProxyStyle(style),
        icon_{ icon }
    {}

    virtual void drawControl(ControlElement element, const QStyleOption* option,
        QPainter* painter, const QWidget* widget = 0) const
    {
        if(element == QStyle::CE_DockWidgetTitle)
        {
            const int iconSize{ pixelMetric(QStyle::PM_ToolBarIconSize) };
            const int margin{ baseStyle()->pixelMetric(QStyle::PM_DockWidgetTitleMargin) };

            const QRect optionRect{ option->rect };

            if (optionRect.width() > optionRect.height())
            {
                QPoint iconPoint{ margin + optionRect.left(),
                                  margin + optionRect.center().y() - iconSize / 2 };
                painter->drawPixmap(iconPoint, icon_.pixmap(iconSize, iconSize));
                const_cast<QStyleOption*>(option)->rect = optionRect.adjusted(iconSize, 0, 0, 0);
            }
            else
            {
                QPoint iconPoint{ margin + optionRect.center().x() - iconSize / 2,
                                  margin + optionRect.bottom() - iconSize };
                painter->drawPixmap(iconPoint, icon_.pixmap(iconSize, iconSize));
                const_cast<QStyleOption*>(option)->rect = optionRect.adjusted(0, 0, 0, -iconSize);
            }
        }

        baseStyle()->drawControl(element, option, painter, widget);
    }

private:
    QIcon icon_;
};
*/
class DryDockWidget: public QDockWidget
{
    Q_OBJECT

public:
    explicit DryDockWidget(DryWidget* widget, QWidget* parent = nullptr);
    void applyWidget(DryWidget* newWidget);

    void updateTitle();
    
public slots:
    void resizeEvent(QResizeEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

private slots:
    void onTopLevelChanged(bool topLevel);

private:
    DryWidget* dryWidget_;
};

#endif // DRYDOCKWIDGET_H
