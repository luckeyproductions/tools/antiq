/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDir>
#include "weaver.h"
#include "qocoon.h"
#include "project.h"

Project::Project(Context* context) : Serializable(context),
    location_{},
    name_{ "Untitled" },
    resourceFolders_{}
{
//    DRY_ATTRIBUTE("location", String, location_, "", AM_EDIT);
//    DRY_ATTRIBUTE("name", String, name_, "", AM_FILE);
//    DRY_ATTRIBUTE("resourceFolders", StringVector, resourceFolders_, StringVector{}, AM_FILE);
}

void Project::OnSetAttribute(const AttributeInfo& attr, const Variant& src)
{
    Serializable::OnSetAttribute(attr, src);
}

bool Project::Save() const
{
    assert(!location_.IsEmpty());
    assert(QDir{ toQString(location_) }.exists());

    SharedPtr<XMLFile> projectFile{ new XMLFile(context_) };
    XMLElement projectRoot{ projectFile->CreateRoot("project") };

    return SaveXML(projectRoot) && projectFile->SaveFile(location_ + FILENAME_PROJECT);
}

bool Project::LoadXML(const XMLElement& source)
{
    name_ = source.GetString("name");

    if (name_.IsEmpty())
        return false;

    for (const String& folder: resourceFolders_)
        removeResourceFolder(folder, false);

    XMLElement folderElem{ source.GetChild("folder") };

    while(folderElem)
    {
        addResourceFolder(folderElem.GetString("path"), false);
        folderElem = folderElem.GetNext("folder");
    }

    return true;
}

bool Project::SaveXML(XMLElement& dest) const
{
    if (!dest.SetString("name", name_))
        return false;

    for (const String& folder: resourceFolders_)
    {
        if (folder.IsEmpty())
            continue;
        else if (!dest.CreateChild("folder").SetString("path",
                                                       folder.Replaced(location(), "")))
            return false;
    }

    return true;
}

void Project::ApplyAttributes()
{
}

bool Project::setLocation(const String& path)
{
    assert(path.EndsWith("/"));

    if (path == location_)
        return false;

    location_ = path;
    return true;
}

bool Project::setName(const String& name)
{
    if (name.IsEmpty() || name == name_)
        return false;

    name_ = name;
    return true;
}

bool Project::addResourceFolder(const String& path, bool emitChange)
{
    if (path.IsEmpty())
        return false;

    String resourceFolder{ AddTrailingSlash(path) };

    if (resourceFolders_.Contains(resourceFolder))
        return false;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    const unsigned priority{ !inProjectFolder(path) + 1 };

    if (!cache->AddResourceDir(absoluteResourceFolder(path), priority))
        return false;

    resourceFolders_.Push(resourceFolder);

    if (emitChange)
        emit GetSubsystem<Qocoon>()->resourceFoldersChanged();

    return true;
}

bool Project::removeResourceFolder(const String& path, bool emitChange)
{
    if (path.IsEmpty() ||
        !resourceFolders_.Remove(localizedPath(path)))
    {
        return false;
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    cache->ReleaseResources(path, true);
    cache->RemoveResourceDir(absoluteResourceFolder(path));

    if (emitChange)
        emit GetSubsystem<Qocoon>()->resourceFoldersChanged();

    return true;
}

String Project::absoluteResourceFolder(const String& path) const
{
    const bool absolute{ IsAbsolutePath(path) };

    return absolute ? path : (location() + path);
}

String Project::localizedPath(const String& path) const
{
    return AddTrailingSlash(path.Replaced(location(), ""));
}

bool Project::inResourceFolder(const String& fileName)
{
    for (const String& folder: resourceFolders())
    {
        if (fileName.StartsWith(absoluteResourceFolder(folder)))
            return true;
    }

    return false;
}

bool Project::inProjectFolder(const String& path)
{
    return absoluteResourceFolder(path).StartsWith(location_);
}

void Project::remove()
{
    for (const String& folder: resourceFolders_)
        removeResourceFolder(folder);
}
