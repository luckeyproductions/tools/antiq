#include "pixelview.h"
#include "view2d.h"
#include "view3d.h"

#include "hololith.h"

Hololith::Hololith(Context* context, QWidget* parent): QStackedWidget(parent),
    off_{ new PreView{ context, this } },
    pixelView_{ new PixelView{ this } },
    view2d_{ new View2D{ this } },
    view3d_{ new View3D{ context } }
{
    QPalette palette{};
    palette.setBrush(QPalette::Background, QPixmap{ QString{ ":/Wacu" } });

    setPalette(palette);

    addWidget(off_);
    addWidget(pixelView_);
    addWidget(view2d_);
    addWidget(view3d_);
}

PreView::PreView(Context* context, QWidget* parent): QWidget(parent)
{
    QPalette palette{};
    palette.setBrush(QPalette::Background, QPixmap{ QString{ ":/Wacu" } });
    setPalette(palette);
    setMinimumSize( QSize{ 01, 01 } );
}
