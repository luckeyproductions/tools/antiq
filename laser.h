#ifndef LASER_H
#define LASER_H

#include "qpit.h"

#include <QLabel>
#include <QToolBar>

class Plasme;
class Laser;

class Viser: public QLabel
{
    Q_OBJECT
#define CELL UNIT * 02

    friend class Laser;

    explicit Viser(Laser* parent = nullptr);

    QPoint origin() const { return -QPoint{ viewWidth_ / 02 - 01, viewHeight_ / 02 }; }
    void updateView();
    void repaintBackground();

    Laser* laser_;

    unsigned viewWidth_;
    unsigned viewHeight_;

    QPixmap background_;
    QPixmap view_;
};

struct Scout
{
    QPoint pos_;
    Direction aim_;

    void step();
};

class Laser: public QToolBar
{
    Q_OBJECT

    friend class Viser;

public:
    Laser(QWidget* parent);

    QPoint scout() const { return scout_; }

    void burn(unsigned char c, Channel chan = FULL, bool walk = true);
signals:
    void fire(QPoint cell, unsigned char c, Channel d = ALPHA, bool m = false);

public slots:
    void target(Plasme* p);
    void target(QPoint p, Direction aim, bool mir = false);
    void turn(int d)
    {
        aim_ = static_cast<Direction>(aim_ + d);
    }

    void updateViser() { viser_->updateView(); }

private:
    QSize visibleSize() const;

    QPoint scout_;
    std::vector<QPoint> return_;
    Direction aim_;
    Viser* viser_;
};

#endif // LASER_H
