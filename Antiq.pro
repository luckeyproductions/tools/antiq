TEMPLATE = app
TARGET = antiq

CONFIG += console c++11
CONFIG -= app_bundle
QT += core gui widgets
QMAKE_CXXFLAGS += -std=c++17 -O2

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -L../.build-Witch-Desktop-Release/ -lWitch \
    -lpthread \
    -ldl \
    -lGL

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \
    Armer \
    witch

HEADERS += \
    Armer/anvil.h \
    Armer/handles.h \
    flux.h \
    laser.h \
    qpdefs.h \
    qpit.h \
    view2d.h \
    weaver.h \
    qocoon.h \
    plasme.h \
    hololith.h \
    holoview.h \
    pixelview.h \
    view3d.h \
    dry.h \
    jib.h \
    lumwidget.h \
    specter.h \
    Armer/armer.h \
    Armer/curvebar.h \
    snail.h \
\
    drywidget.h \
    filewidget.h \
    drydockwidget.h \
    project.h \

SOURCES += \
    Armer/anvil.cpp \
    Armer/handles.cpp \
    flux.cpp \
    laser.cpp \
    main.cpp \
    qpit.cpp \
    view2d.cpp \
    weaver.cpp \
    qocoon.cpp \
    plasme.cpp \
    hololith.cpp \
    holoview.cpp \
    pixelview.cpp \
    view3d.cpp \
    jib.cpp \
    lumwidget.cpp \
    specter.cpp \
    Armer/armer.cpp \
    Armer/curvebar.cpp \
    snail.cpp \
\
    drywidget.cpp \
    filewidget.cpp \
    drydockwidget.cpp \
    project.cpp \

DISTFILES += \
    .gitignore

RESOURCES += \
    antiq.qrc
