/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>
#include "qocoon.h"
#include "view3d.h"

#include "weaver.h"

Context* Weaver::context_s{ nullptr };

Weaver::Weaver(Context* context, int & argc, char** argv): QApplication(argc, argv), Application(context),
    argument_{},
    requireUpdate_{ true }, //////
    qocoon_{ nullptr }
{
    context_s = context_;

    std::locale::global(std::locale::classic());

    const QString displayName{ "Antiq" };
    QCoreApplication::setOrganizationName("LucKey Productions");
    QCoreApplication::setOrganizationDomain("luckeyprodutions.nl");
    QCoreApplication::setApplicationName(displayName.toLower());
    QGuiApplication::setApplicationDisplayName(displayName);
}

Weaver::~Weaver()
{
    delete qocoon_;
}

void Weaver::Setup()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    QWidget* fishTrap{ new QWidget{} };
    engineParameters_[EP_EXTERNAL_WINDOW] = (void*)(fishTrap->winId());
    delete fishTrap;

    String manaWargResourcePath{ "Resources" };

    if (!fs->DirExists(fs->GetProgramDir() + manaWargResourcePath))
        manaWargResourcePath = fs->GetAppPreferencesDir("luckey", toString(applicationName())); //Assume Antiq is installed

    engineParameters_[EP_RESOURCE_PATHS] = manaWargResourcePath;
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_LOG_NAME] = fs->GetAppPreferencesDir("luckey", "logs") +
                                     toString(applicationName()) + ".log";
}

void Weaver::Start()
{
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    cache->SetAutoReloadResources(true);
//    SharedPtr<XMLFile> renderPathFile{ cache->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml") };
//    GetSubsystem<Renderer>()->SetDefaultRenderPath(renderPathFile);

    context_->RegisterSubsystem(this);

    context_->RegisterFactory<Jib>();

    if (!argument_.IsEmpty())
        handleArgument();

    if (!qocoon_)
        qocoon_ = new Qocoon{ context_ };

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer.start(1000 / GetSubsystem<Graphics>()->GetRefreshRate());

    exec();
    exit();
}

void Weaver::Stop()
{
    engine_->DumpResources(true);
}
void Weaver::exit()
{
    qocoon_->close();
    engine_->Exit();
}

bool Weaver::looksLikeUrho(QString folder)
{
   return looksLikeUrho(toString(folder));
}

bool Weaver::looksLikeUrho(String folder)
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    return fs->DirExists(folder + "/bin/Data")
        && fs->DirExists(folder + "/bin/CoreData");
}

void Weaver::handleArgument()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    if (!IsAbsolutePath(argument_))
    {
        while (argument_.StartsWith("../")) // Resolve basic relativity
        {
            fs->SetCurrentDir(GetParentPath(fs->GetCurrentDir()));
            argument_ = argument_.Substring(3);
        }

        argument_ = fs->GetCurrentDir() + argument_;
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(argument_) };

    if (xmlFile)
    {
        const XMLElement& rootElem{ xmlFile->GetRoot() };
        String elemName{ rootElem.GetName() };
        String resourceRoot{ Qocoon::locateResourceRoot(argument_) };

        if (!elemName.IsEmpty())
        {
            if (elemName == "project")
            {
                qocoon_ = new Qocoon(context_);
                qocoon_->loadProject(toQString(resourceRoot + FILENAME_PROJECT));
                return;
            }
/*            else if (elemName == "material")
            {
                SharedPtr<Material> material{ cache->GetResource<Material>(argument_) };

                if (!material.IsNull())
                {
                    qocoon_ = new Qocoon{ context_, material };

                    if (!resourceRoot.IsEmpty())
                        qocoon_->loadProject(toQString(resourceRoot + FILENAME_PROJECT));

                    argument_.Replace(resourceRoot, "");
                    material->SetName(argument_);
                    cache->ReloadResource(material);

                }
            }*/ // else open in text editor
        }
    }
}

void Weaver::onTimeout()
{
    if (requireUpdate_ && !qocoon_->isMinimized()
        && engine_ && !engine_->IsExiting())
    {
        runFrame();
    }
}

void Weaver::runFrame()
{
    requireUpdate_ = false;
    engine_->RunFrame();

    qDebug() << GetSubsystem<Time>()->GetTimeStamp().CString() << "Dry frame";
}
