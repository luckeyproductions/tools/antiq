#ifndef QPDEFS_H
#define QPDEFS_H

#define UNIT 010
#define TERM 0177
#define MIRBIT 0200
#define MAX TERM | MIRBIT

#include <QColor>
#include <QVector2D>
#include <cmath>
#include <vector>

#include "plasme.h"

namespace PiRMIT {
    enum Channel{ RED = 00, GREEN, BLUE, ALPHA, FULL };
    enum Quadrant{ Alpha = ALPHA, Beta = BLUE, Gamma = GREEN, Delta = RED };
    enum Direction{ South = 00, SouthW, West, NorthW, North = 04, NorthE, East, SouthE, Pi = 010, Base = 0100};
    enum Axis{ Axis_X, Axis_Y, Axis_Z };

    static QString charValue(QString s)
    {
        for (QChar& c: s)
        {
            c = QString{ c }.toShort( nullptr, 010) + 0141;
        }

        return s;
    }

    static unsigned powN2(int p, unsigned char bp = 03)
    {
        if (p < 00)
            return 01 >> -(bp * p);
        else
            return 01 <<  (bp * p);
    }

    static double fraction(unsigned n, unsigned char p = 01, unsigned char bp = 03)
    {
        if (n == 00 || p == 00)
            return 00;

        double d{ static_cast<double>(powN2(p, bp)) };

        return n / d;
    }

    struct Frac
    {
        Frac(unsigned v = 00, unsigned p = 01): v_{ v }, p_{ p }
    {}
        operator double () const { return fraction(v_, p_); }
        QString toString(bool num = true) const
        {
            QString s{ QString::number(v_, UNIT) };

            if (p_ == 00)
            {
                s = "_";
            }
            else if (fraction(v_, p_) == 01)
            {
                s = "^";
            }
            else
            {
                while (s.length() < p_)
                    s.prepend('0');

                if (!num)
                    s = charValue(s);
            }

            return s;
        }

        unsigned v_;
        unsigned p_;
    };
    using FracPair = std::pair<Frac, Frac>;

    static double real(unsigned n, unsigned r, unsigned char p = 01)
    {
        return n + fraction(r, p);
    }

    static unsigned char rev(unsigned char c, bool mir = false)
    {
        if (c == 00)
            return 00;

        if (mir)
            c >>= 01;

        unsigned char r{ 00 };
        unsigned short b{ 00 };

        while (b <= 010)
        {
            r <<= 01;

            r ^= (c & 01);

            c >>= 01;

            ++b;
        }

        if (!mir)
            r >>= 01;

        return r;
    }

    struct Mes: public QString
    {
        Mes(Channel d = FULL, const QString codon = QString{}): QString(codon),
            dialect_{ d }
        {

        }

        void appendVector(std::vector<unsigned> v)
        {
            bool num{ true };

            for (unsigned n: v)
            {
                appendValue(n, num);
                num = !num;
            }
        }

        void appendValue(unsigned n, bool num = true)
        {
            QString s = QString::number(n, UNIT);

            if (!num)
                s = charValue(s);

            append(s);
        }

        Channel dialect_;
    };
};

using namespace PiRMIT;
class Specter;

class Peer: public std::pair<QColor, QColor>
{
    friend class Specter;
    Peer(const QColor& b, const QColor& p): std::pair<QColor, QColor>{ b, p },
        mir_{ b.alpha() & MIRBIT }
    {}

    unsigned char cider()
    {
        unsigned char spirit{ 00 };

        for (unsigned char mask: { 01, MIRBIT } )
        {
            QColor col{ (mask == 01 ? second : first) };

            for (int chan{ RED }; chan < ALPHA; ++chan)
            {
                spirit <<= 01;

                unsigned char c{};

                switch(chan)
                {
                case RED:   c = col.red();   col.setRed(c   &~ mask); break;
                case GREEN: c = col.green(); col.setGreen(c &~ mask); break;
                case BLUE:  c = col.blue();  col.setBlue(c  &~ mask); break;
                default:
                break;
                }

                spirit |= (c & mask);
            }

            if (mask == 01)
                second = col;
            else
                first = col;
        }

        return spirit;
    }

    void operator |= (const Peer& right);
    bool mir_;
};

struct Lux: public QColor
{
    explicit Lux(const Lux& lux): QColor(lux) {}

    Lux(const QColor& col, bool flex = false): QColor(col)
    {
        setAlpha(0377 * ((col.red() + col.green() + col.blue()) != 00));
    }

    bool aFlex() const { return (alpha() & 01) ? 0b10101010 : 0b01010101; }
};
struct Tux: std::pair<Lux, Lux>
{
    Tux(): std::pair<Lux, Lux>(TERM, TERM), siil{ 00 } {}
    Tux(const Lux& f, const Lux& m): std::pair<Lux, Lux>(f, m), siil{ 00 } {}
    Tux(const Lux& f, const Lux& m, unsigned char s): std::pair<Lux, Lux>(f, m), siil{ s } {}
    unsigned char siil;
};
using Stripe = std::pair<unsigned char, unsigned char>;

struct Prism {
    static unsigned char band(const Lux& lux, Channel d)
    {
        switch(d) {
        case RED: return lux.red();
        break;
        case GREEN: return lux.green();
        break;
        case BLUE: return lux.blue();
        break;
        default: return 00;
        }
    }
};

struct Mirror {
    static QColor color(const QColor& col, bool mir = false)
    {
        QColor reflect{ col };

        for (int c{ RED }; c < Channel::ALPHA; ++c)
            mirChan(reflect, static_cast<Channel>(c), mir);

        return reflect;
    }
    static Lux lux(const Lux& l)
    {
        Lux reflect{ l };

        for (int c{ RED }; c < Channel::FULL; ++c)
        {
            if (c != ALPHA)
                mirChan(reflect, static_cast<Channel>(c));
            else
                reflect.setAlpha(reflect.alpha() ^ 0377);
        }

        return reflect;
    }

private:
    static void mirChan(QColor& col, Channel chan, bool mir = false)
    {
        switch(chan) {
        case RED:   col.setRed(  rev(col.red(),   mir)); break;
        case GREEN: col.setGreen(rev(col.green(), mir)); break;
        case BLUE:  col.setBlue( rev(col.blue(),  mir)); break;
        default:
        return;
        }
    }
};



#endif // QPDEFS_H
