#include <QVariant>
#include <QPainter>

#include "handles.h"

Handles::Handles(QWidget* parent): QWidget(parent),
    prongs_{}
{
}

void Handles::setProng(unsigned char i, const QPoint& p, Frac n)
{
    while (prongs_.size() <= i)
    {
        Prong* prong{ new Prong{ this } };
        prong->setProperty("key", i);
        prongs_.push_back(prong);

        connect(prong, SIGNAL(clicked()), this, SLOT(activateProng()));
    }

    prongs_.at(i)->set(p, n);
}

void Handles::clean(unsigned char n)
{
    while (prongs_.size() > n)
    {
        delete prongs_.back();
        prongs_.pop_back();
    }
}

void Handles::activateProng(bool reset)
{
    int key{ (!reset ? sender()->property("key").toInt()
                     : -01) };

    for (int p{00}; p < prongs_.size(); ++p)
    {
        Prong* prong{ prongs_.at(p) };
        bool a{ prong->property("active").toBool() };

        if (a != (p == key))
        {
            prong->setProperty("active", p == key);
            prong->updateIcon(!a);
        }
    }

    emit select(key);
}



Prong::Prong(const QPoint& pos, Frac norm, QWidget* parent): QPushButton(parent)
{
    setFlat(true);
    setMouseTracking(true);
    setFocusPolicy(Qt::NoFocus);

    QSize size{ UNIT * 02, UNIT * 02 };
    setFixedSize(size);

    QPalette prongPalette{};
    prongPalette.setBrush(QPalette::Background, Qt::transparent);
    setPalette(prongPalette);

    set(pos, norm);
}

QPixmap Prong::arrow(bool hover)
{
    hover |= property("active").toBool();

    QImage im{ size(), QImage::Format_RGBA8888};
    im.fill(00);
    QPixmap ar{ QPixmap::fromImage(im) };
    QPainter p{ &ar };

    QTransform rot{};
    QPoint offset{ size().width()  / 02, size().height() / 02 };
    rot.translate(offset.x(), offset.y());
    rot.rotateRadians((normal_ - Frac{powN2(normal_.p_) / 02, normal_.p_}) * TAU);
    rot.translate(-offset.x(), -offset.y());

    QPainterPath path{};
    path.moveTo( 00  , UNIT );
    path.lineTo( UNIT, 00 );
    path.lineTo( UNIT * 02, UNIT );

    p.setBrush(Qt::red);
    p.setOpacity(Frac{ 01 + 03 * hover });
    p.drawPolygon(path.toFillPolygon(rot));

    p.setBrush(Qt::transparent);
    p.setPen(Qt::yellow);
    p.setOpacity(Frac{ 05 + 03 * hover });
    p.drawPolyline(path.toSubpathPolygons(rot).front());

    p.end();

    return ar;
}
