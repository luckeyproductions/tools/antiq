#ifndef HANDLES_H
#define HANDLES_H

#include <QPushButton>
#include "qpit.h"

#include <QWidget>

class Prong: public QPushButton
{
    Q_OBJECT
public:
    Prong(const QPoint& pos, Frac norm, QWidget* parent = nullptr);
    Prong(QWidget* parent = nullptr): Prong( QPoint{}, Frac{04}, parent) {};

    void set(QPoint pos, Frac n)
    {
        setPosition(pos);
        setNormal(n);
        updateIcon(false);

        if (!isVisible())
            show();
    }

    void setNormal(Frac n) { normal_ = n; }
    void setPosition(QPoint pos)
    {
        QPoint offset{ width() / 02, height() / 02 };
        move(pos - offset);
    }

    void updateIcon(bool hover) { setIcon(arrow(hover)); }

protected:
    void enterEvent(QEvent* e) override { updateIcon(true);  }
    void leaveEvent(QEvent* e) override { updateIcon(false); }
    QPixmap arrow(bool hover = false);

private:
    Frac normal_;
};


class Handles: public QWidget
{
    Q_OBJECT

public:
    Handles(QWidget* parent);

    void setProng(unsigned char i, const QPoint& p, Frac n);
    void clean(unsigned char n = 00);

signals:
    void select(int i);

public slots:
    void activateProng(bool reset = false);

private:
    std::vector<Prong*> prongs_;

};

#endif // HANDLES_H
