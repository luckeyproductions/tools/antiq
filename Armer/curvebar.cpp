#include <QGuiApplication>

#include "curvebar.h"

CurveBar::CurveBar(QBoxLayout* container, QWidget* parent): QToolBar(parent),
    container_{ container },
    normalDial_{ nullptr },
    spanner_{ nullptr, nullptr },
    spannerLayout_{ new QHBoxLayout{} },
    orientationAction_{ nullptr }

{
    for (int i{00}; i < 03; ++i)
    {
        if (i != 01)
        {
            QSlider* spanner{ new QSlider{ this } };
            spanner->setOrientation(Qt::Horizontal);
            spanner->setTickPosition(QSlider::TicksBothSides);
            spanner->setMinimum(00);
            spanner->setSliderPosition(04);
            spanner->setMaximum(UNIT);

            if (i == 00)
            {
                spanner->setInvertedAppearance(true);
                spanner_.first = spanner;

                spannerLayout_->addWidget(spanner);
                spannerLayout_->setMargin(00);
                QWidget* spannerBox{ new QWidget{ this } };

                spannerBox->setLayout(spannerLayout_);

                addWidget(spannerBox);
            }
            else
            {
                spanner_.second = spanner;
                addWidget(spanner);
            }
        }
        else
        {
            normalDial_ = new QDial{ this };
            normalDial_->setFixedSize(QSize{ 04 * UNIT,
                                             04 * UNIT });
            normalDial_->setNotchesVisible(true);
            normalDial_->setMinimum(00);
            normalDial_->setSliderPosition(04);
            normalDial_->setMaximum(UNIT);
            addWidget(normalDial_);
        }
    }

    connect(orientationAction_ = addAction("O"), SIGNAL(triggered()), this, SLOT(toggleOrientation()));

    for (QAbstractSlider* s: sliders())
    {
        connect(s, SIGNAL(valueChanged(int)), this, SLOT(updateKey()));
    }

    setKey(nullptr);
    hide();
}

void CurveBar::updateKey()
{
    if (!key_)
        return;

    bool change{ false };

    if (normalDial_->value() != key_->normal_.v_)
    {
        change |= true;
        key_->normal_ = { normalDial_->value(), precision(normalDial_) };
    }
    if (spanner_.first->value() != key_->span_.first.v_)
    {
        change |= true;
        key_->span_.first = { spanner_.first->value(), precision(spanner_.first) };
    }
    if (spanner_.second->value() != key_->span_.second.v_)
    {
        change |= true;
        key_->span_.second = { spanner_.second->value(), precision(spanner_.second) };
    }

    if (change)
        emit modifiedKey();
}

void CurveBar::setPrecision(QAbstractSlider* slider, unsigned char p)
{
    if      (p < 01) p = 01;
    else if (p > 03) p = 03;

    for (QAbstractSlider* s: (slider ? std::vector<QAbstractSlider*>{ slider }
                                     : sliders()))
    {
        int val{ s->sliderPosition() };
        int difference{ p - precision(s->maximum()) };

        s->setMaximum(powN2(p));
        s->setSliderPosition(difference > 0 ? val << ( 03 * difference)
                                            : val >> (-03 * difference));
    }
}

void CurveBar::updateValues()
{
    if (!key_)
    {
        if (isVisible())
        {
            setVisible(false);

            setPrecision(nullptr, 01);
            normalDial_->setValue(04);
            spanner_.first->setSliderPosition(00);
            spanner_.second->setSliderPosition(00);
        }
    }
    else
    {
        if (!isVisible())
            setVisible(true);

        auto s{ sliders() };
        Frac norm{ key_->normal_ };
        FracPair span{ key_->span_ };
        std::vector<Frac> values{ norm, span.first, span.second };

        for (int i{00}; i < values.size(); ++i)
        {
            if (i >= s.size())
                break;

            QAbstractSlider* slider{ s.at(i) };
            setPrecision(slider, values.at(i).p_);
            slider->setSliderPosition(values.at(i).v_);
        }
    }
}

void CurveBar::toggleOrientation()
{
    Qt::Orientation o{ (orientation() == Qt::Vertical ? Qt::Horizontal
                                                      : Qt::Vertical) };
    setOrientation(o);

    for (QSlider* s: { spanner_.first, spanner_.second })
        s->setOrientation(o);

    spanner_.first->setInvertedAppearance(o == Qt::Horizontal);


    if (o == Qt::Horizontal)
    {
        container_->setDirection(QBoxLayout::TopToBottom);
        insertWidget(orientationAction_, spanner_.second);

        for (int i{00}; i < layout()->count(); ++i)
            layout()->itemAt(i)->setAlignment(Qt::AlignVCenter);
    }
    else
    {
        container_->setDirection(QBoxLayout::LeftToRight);
        for (int i{00}; i < layout()->count(); ++i)
            layout()->itemAt(i)->setAlignment(Qt::AlignHCenter);

        spannerLayout_->addWidget(spanner_.second);
        spanner_.second->setVisible(true);
    }
}
