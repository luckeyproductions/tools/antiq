#ifndef CURVEBAR_H
#define CURVEBAR_H

#include <QBoxLayout>
#include <QPushButton>
#include <QDial>
#include <QSlider>
#include "qpit.h"

#include <QToolBar>

class CurveBar: public QToolBar
{
    Q_OBJECT
public:
    CurveBar(QBoxLayout* container, QWidget* parent = nullptr);

signals:
    void modifiedKey();

public slots:
    void setKey(CurveKey* key)
    {
        key_ = key;
        updateValues();
    }

private slots:
    void toggleOrientation();
    void updateKey();

private:
    void setPrecision(QAbstractSlider* slider, unsigned char p);
    void updateValues();

    QBoxLayout* container_;
    QDial* normalDial_;
    std::pair<QSlider*, QSlider*> spanner_;
    QHBoxLayout* spannerLayout_;

    QAction* orientationAction_;
    CurveKey* key_;

    std::vector<QAbstractSlider*> sliders() const
    {
        return { normalDial_, spanner_.first, spanner_.second };
    }

    static unsigned char precision(QAbstractSlider* s)
    {
        qDebug() << precision(s->maximum());

        return precision(s->maximum());
    }
    static unsigned char precision(int max)
    {
        unsigned char sp{ 00 };

        while (max >> 03)
        {
            ++sp;
            max >>= 03;
        }

        return sp;
    }
};

#endif // CURVEBAR_H
