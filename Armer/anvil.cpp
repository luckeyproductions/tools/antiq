/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPainter>
#include "Armer/armer.h"

#include "anvil.h"

Anvil::Anvil(Armer* parent): QWidget(parent),
    armer_{ parent },
    handles_{ new Handles{ this } },
    volume_{ Volume::sphere() },
    spinAxis_{ Axis_Z },
    spin_{ 04, 01 }
{
    connect(handles_, SIGNAL(select(int)), armer_, SLOT(selectCurveKey(int)));

    refresh();
}

int Anvil::gridInt() const { return armer_->gridInt(); }

const RedMes Anvil::hammer()
{
    RedMes mes{ volume_ };

    //
    volume_ = Volume::random();
    refresh();
    handles_->activateProng(true);
    qDebug() << "\n=θ *ting*\n" << mes;
    //

    return mes;
}

void Anvil::refresh()
{
    setFixedSize( UNIT * armer_->scale() * QSize{ box().length_ + 02, box().depth_ + 02 });
    handles_->setFixedSize(size());

    QPalette anvilPalette{};
    anvilPalette.setBrush(QPalette::Background, background());
    setPalette(anvilPalette);
}

QPixmap Anvil::background()
{
    QImage lines{ size(), QImage::Format_RGBA8888 };
    lines.fill(00);

    drawOrigin(&lines);
    drawOutline(&lines);
    drawCurves(&lines);
    drawTrunk(&lines);

    return QPixmap::fromImage(lines);
}

void Anvil::drawOrigin(QImage* lines)
{
    QPainter p{ lines };
    QPen pen{ Qt::yellow };
    p.setPen(pen);

    p.drawEllipse(origin(),
                  UNIT / 02, UNIT / 02);
    p.end();
}

void Anvil::drawTrunk(QImage* lines)
{
    const int gInt{ armer_->gridInt() };

    QPainter p{ lines };
    QPen pen{ QColor{ 0200, 0200, 0200, 0377 } };
    pen.setWidth(02);
    p.setPen(pen);

    p.drawLine( QPoint{ gInt, vMid() },
                QPoint{ gInt + gInt * box().length_, vMid() });
    p.end();
}

void Anvil::drawOutline(QImage* lines)
{
    const int gInt{ gridInt() };

    QPainter p{ lines };

    QPen pen{ Qt::red };
    pen.setStyle(Qt::DotLine);
    p.setPen(pen);

    p.drawRect(QRect{ QPoint{ gInt, gInt },
                      QSize{ box().length_, box().depth_} * gInt });
    p.end();
}

void Anvil::drawCurves(QImage* lines)
{
    const int gInt{ gridInt() };

    PiRMIT::Curve::Keys& keys{ surface().front().keys_ };
    handles_->clean(keys.size());

    QPainter p{ lines };
    QPen pen{ Qt::yellow };
    p.setPen(pen);

    QPainterPath path{};
    QVector2D prevPos{ gInt, vMid() };

    for (int k{ 00 }; k < keys.size(); ++k)
    {
        const CurveKey& key{ keys.at(k) };
        const int valPosY{ vMid() - gInt * (key.value_ * box().depth_) / 02 };

        QVector2D pos{ gInt, valPosY };
        QVector2D norm{ QPit::direction(key.normal_) };
        norm.setY(-norm.y());

        if (k == 00)
        {
            path.moveTo( pos.toPoint() );
            handles_->setProng(k, pos.toPoint(), key.normal_);

            if (keys.size() == 01)
            {
                prevPos = pos;
                k = 01;
            }
        }

        if (k != 00)
        {
            pos.setX(gInt + el(k, keys.size()));

            if (keys.size() != 01)
            {
                handles_->setProng(k, pos.toPoint(), key.normal_);
            }

            p.setOpacity(Frac{03});

            const FracPair normals{ keys.at(k - 01).normal_, key.normal_ };
            const FracPair spanner{ keys.at(k - 01).span_.second, key.span_.first };

            const std::pair<QVector2D, QVector2D> control{
                prevPos + QPit::direction(normals.first , true) * el(spanner.first , keys.size()),
                    pos - QPit::direction(normals.second, true) * el(spanner.second, keys.size())
            };

            pen.setStyle(Qt::DashLine);
            p.setPen(pen);
            p.drawLine(prevPos.toPoint(), control.first.toPointF());
            p.drawLine(pos.toPoint(), control.second.toPointF());
            pen.setStyle(Qt::SolidLine);

            path.cubicTo(control.first.toPointF(), control.second.toPointF(), pos.toPoint());
            prevPos = pos;

            if (key.span_.first.v_ + key.span_.second.v_ == 00)
                norm = QVector2D{};
        }


        p.setOpacity(Frac{03});
        p.drawLine(pos.toPoint(), (pos + norm * gInt / 02).toPoint());
        p.setOpacity(01);
    }

//    path.lineTo(gInt + gInt * box().length_, vMid());
//    QPainterPath clip{};
//    clip.addRect(QRect{ QPoint{}, QSize{ gInt + gInt * box().length_, vMid() + 01 } });
//    path = path.intersected(clip);
///
    QPainterPath quickPath{ surface().front().painterPath(gInt * QVector2D{ box().length_, box().depth_ }, QVector2D{ gInt, vMid() }) };
    std::vector<float> zp{};
    QPit::zeroPoints(&path, zp, vMid());

    p.setOpacity(01);
    pen.setStyle(Qt::SolidLine);
    p.setPen(pen);
    p.drawPath(quickPath);

    pen.setColor(Qt::red);
    p.setPen(pen);
    for (float x: zp)
        p.drawLine(QPointF{ x, vMid() - UNIT / 02 },
                   QPointF{ x, vMid() + UNIT / 02 });

    pen.setColor(Qt::yellow);
    pen.setStyle(Qt::DotLine);
    p.setPen(pen);

    QTransform mirror{};
    mirror.scale(1.0, -1.0);
    mirror.translate(0.0, -height());
    p.setTransform(mirror);
    p.drawPath(quickPath);

    p.end();
}
