/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ANVIL_H
#define ANVIL_H

#include "qpit.h"
#include "Armer/handles.h"

#include <QWidget>

class Armer;

class Anvil: public QWidget
{
    Q_OBJECT

public:
    Anvil(Armer* parent = nullptr);

    const RedMes hammer();

    CurveKey& key(unsigned i) { return volume_.radius_.front().at(i); }

public slots:
    void refresh();

private:
    QPixmap background();
    void drawOrigin(QImage* lines);
    void drawTrunk(QImage* lines);
    void drawOutline(QImage* lines);
    void drawCurves(QImage* lines);

    PiRMIT::Box&     box()     { return volume_.box_;    }
    Volume::Surface& surface() { return volume_.radius_; }
    PiRMIT::Curve&   trunk()   { return volume_.trunk_;  }

    unsigned el(const double x, const unsigned n)
    {
        return gridInt() * (x / std::max(01u, n - 01u)) * box().length_;
    }

    const Armer* const armer_;
    Handles* const handles_;

    Volume volume_;
    Axis spinAxis_;
    Frac spin_;

    int   gridInt() const;
    int      vMid() { return gridInt() + gridInt() * box().depth_ / 02; }
    QPointF origin() { return QPoint{ gridInt(), vMid() }; }
};

#endif // ANVIL_H
