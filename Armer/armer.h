/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ARMER_H
#define ARMER_H

#include "lumwidget.h"

class Anvil;
class CurveBar;

class Armer: public LumWidget
{
    Q_OBJECT

public:
    explicit Armer(QWidget* parent = nullptr);

signals:
    void hammer(const Mes mes);
public slots:
    void selectCurveKey(int i);

protected:
    void setScale(char z) override;

protected slots:
    void apply();
    void zoomIn();
    void zoomOut();

private:
    Anvil* anvil_;
    CurveBar* curveBar_;
};

#endif // ARMER_H
