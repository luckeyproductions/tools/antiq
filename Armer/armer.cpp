/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "qocoon.h"
#include "specter.h"

#include "Armer/armer.h"
#include "Armer/anvil.h"
#include "Armer/curvebar.h"

Armer::Armer(QWidget* parent): LumWidget(parent),
    anvil_{ new Anvil{ this } },
    curveBar_{ nullptr }
{
    setObjectName("Armer");
    defaultScale_ = 01;
    setScale(defaultScale_);

    connect(toolBar_->addAction("Apply"), SIGNAL(triggered()), this, SLOT(apply()));
    toolBar_->addWidget(SPACER);

    QAction* zoomOutAction{ toolBar_->addAction("-") };
    QAction* zoomInAction{  toolBar_->addAction("+") };
    zoomOutAction->setShortcut(QKeySequence{"-"});
    zoomInAction->setShortcut(QKeySequence{"+"});
    connect(zoomOutAction, SIGNAL(triggered()), this, SLOT(zoomOut()));
    connect(zoomInAction,  SIGNAL(triggered()), this, SLOT(zoomIn()));


    QVBoxLayout* bottomLayout{ new QVBoxLayout{} };
    curveBar_ = new CurveBar{ bottomLayout, this };
    bottomLayout->addWidget(lumArea_);
    bottomLayout->addWidget(curveBar_);
    static_cast<QVBoxLayout*>(layout())->addLayout(bottomLayout);

    lumArea_->setWidget(anvil_);
    anvil_->refresh();

    connect(curveBar_, SIGNAL(modifiedKey()), anvil_, SLOT(refresh()));
}

void Armer::zoomIn()
{
    if (!contains(QCursor::pos()))
        return;

    setScale(++scaleFactor_);
}
void Armer::zoomOut()
{
    if (!contains(QCursor::pos()))
        return;

    setScale(--scaleFactor_);
}

void Armer::setScale(char z)
{
    LumWidget::setScale(z);
    anvil_->refresh();
}

void Armer::selectCurveKey(int i)
{
    if (i < 00)
        curveBar_->setKey(nullptr);
    else
        curveBar_->setKey(&anvil_->key(i));
}

void Armer::apply()
{
    RedMes mes{ anvil_->hammer() };
    selectCurveKey(-01);

    emit hammer(mes);
}
