#include <QGridLayout>
#include <QLineEdit>
#include "plasme.h"
#include "laser.h"

#include "specter.h"

Specter::Specter(QWidget* parent): LumWidget(parent),
    laserBar_{ new Laser(this) },
    sheet_{ new Sheet(this) },
    visible_{ nullptr }
{
    setObjectName("Specter");

    for (QString name: { "R", "G", "B", "A" })
    {
        QAction* action{ toolBar_->addAction(name) };
        action->setCheckable(true);

    }
    for (bool laser: { true, false })
    {
        QString name{ (laser ? "Laser" : "Lock") };

        QAction* action{ toolBar_->addAction(name) };
        action->setCheckable(true);

        if (laser)
        {
            action->setChecked(true);
            connect(action, SIGNAL(triggered(bool)), laserBar_, SLOT(setVisible(bool)));
        }
    }

    toolBar_->addWidget(SPACER);
    connect(toolBar_->addAction("-"), SIGNAL(triggered()), this, SLOT(zoomOut()));
    connect(toolBar_->addAction("+"), SIGNAL(triggered()), this, SLOT(zoomIn()));

    layout()->addWidget(laserBar_);

    connect(this, SIGNAL(revealed(Plasme*)), laserBar_, SLOT(target(Plasme*)));
    connect(laserBar_, SIGNAL(fire(QPoint, unsigned char, Channel, bool)),
            this, SLOT(putChar(QPoint, unsigned char, Channel, bool)));

    lumArea_->setWidget(sheet_);

    defaultScale_ = 01;
    setScale(defaultScale_);
}

void Specter::zoomIn()
{
    if (scaleFactor_ < 04)
        setScale(++scaleFactor_);
}
void Specter::zoomOut()
{
    if (scaleFactor_ > 00)
        setScale(--scaleFactor_);
}

void Specter::updateScale()
{
    sheet_->setSize(displaySize());
}

void Specter::resizeEvent(QResizeEvent* e)
{
    LumWidget::resizeEvent(e);

}
bool Specter::reveal(Plasme* p)
{
    if (visible_ == p)
        return true;

    if (p != nullptr && p->size().isEmpty())
        return false;

    visible_ = p;

    if (visible_ == nullptr)
    {
        sheet_->project(nullptr, nullptr);
        return true;
    }

    sieve(visible_);
    sheet_->project(&firstSide_, &mirSide_);
    updateScale();

    emit revealed(p);

    return true;
}

unsigned char Specter::charAt(const QPoint loc, Channel chan, bool front)
{
    if (!visible_ || loc.x() < 00 || loc.y() < 00)
        return 00;

    QImage image { (front ? firstSide_.toImage() : mirSide_.toImage()) };

    switch (chan) {
    case RED:   return image.pixelColor(loc).red();   break;
    case GREEN: return image.pixelColor(loc).green(); break;
    case BLUE:  return image.pixelColor(loc).blue();  break;
    default: return 00;
    }
}

void Specter::putChar(QPoint cell, unsigned char c, Channel d, bool mir)
{
    QImage image { (!mir ? firstSide_.toImage() : mirSide_.toImage()) };
    QColor col{ image.pixelColor(cell) };

    switch (d)
    {
    case RED:   col.setRed(c); break;
    case GREEN: col.setGreen(c); break;
    case BLUE:  col.setBlue(c); break;
    default: return;
    }

    image.setPixelColor(cell, col);

    if (!mir)
        firstSide_.convertFromImage(image);
    else
        mirSide_.convertFromImage(image);
}

void Specter::putMes(Mes mes, bool mir)
{
    for (QChar c: mes)
    {
        laserBar_->burn(c.toLatin1(), mes.dialect_);
    }

    sheet_->project(&firstSide_, &mirSide_);
    updateScale();
    laserBar_->updateViser();
}

void Specter::sieve(Plasme* plasme)
{
    const QImage f{ plasme->first.toImage() };
    const QImage s{ plasme->second.toImage() };
    const QSize size{ plasme->size() };

    QImage front{  size, QImage::Format_RGBA8888 };
    QImage mirror{ size, QImage::Format_RGBA8888 };
    QImage shade{  size, QImage::Format_RGBA8888 };
    QImage ghost{  size, QImage::Format_Grayscale8 };

    front.fill(00);
    mirror.fill(00);
    shade.fill(00);
    ghost.fill(00);

    for (int x{ 00 }; x < size.height(); ++x)
    for (int y{ 00 }; y < size.width() ; ++y)
    {
        const QPoint loc{ x, y };
        Peer p{ f.pixelColor(loc), s.pixelColor(loc) };

//        qDebug() << (p.first.name()  + p.first.alpha() + " | " + p.second.name() + p.second.alpha());
        Tux tux{ riid(p) };
//        qDebug() << (tux.first.name()  + p.first.alpha() + " | " + tux.second.name() + p.second.alpha());

        Stripe copha{ p.first.alpha(), p.second.alpha() };

//        qDebug() << QChar{tux.first.red()};
//        qDebug() << QChar{tux.first.green()};
//        qDebug() << QChar{tux.first.blue()};
        front.setPixelColor(loc, tux.first);
//        qDebug() << QChar{tux.second.red()};
//        qDebug() << QChar{tux.second.green()};
//        qDebug() << QChar{tux.second.blue()};
        mirror.setPixelColor(loc, tux.second);
//        qDebug() << tux.siil;
        ghost.setPixel(loc, tux.siil);

//        qDebug() << "Copha";
//        qDebug() << copha.first;
//        qDebug() << copha.second;
//        const unsigned char aF{ copha.first };
//        const unsigned char aS{ copha.second };
//        QColor alphaQuadrant{ aF, aF + aS, aS,
//                         0377 * ((aF + aS) != 00) };

//        qDebug() << "Alpha Quadrant";
//        qDebug() << alphaQuadrant;
//        shade.setPixelColor(loc, alphaQuadrant);
    }

    firstSide_.convertFromImage(front);
    mirSide_.convertFromImage(mirror);
    ghost_.convertFromImage(ghost);
//    shade_.convertFromImage(shade);
}

Tux Specter::riid(Peer& p)
{
    const bool fMir{  p.first.alpha() & MIRBIT };
    const bool sMir{ p.second.alpha() & MIRBIT };

//    qDebug() << fMir;
//    qDebug() << sMir;
//    qDebug() << p.mir_;


    if (fMir == true && sMir == true) /// True colour
    {
        qDebug() << "True color";
        return Tux{};
    }
    if (fMir == false && sMir == false) /// Typed
    {
        qDebug() << "Typed";
        return Tux{};
    }

//    qDebug() << "Mirror match";

    if (p.first.alpha() < 040)
        return Tux{};

    unsigned char siil{ p.cider() };

    for (int col{ RED }; col < ALPHA; ++col)
    {
        switch(col) {
        case RED:   p.second.setRed(  rev(p.second.red(),   true));
        break;
        case GREEN: p.second.setGreen(rev(p.second.green(), true));
        break;
        case BLUE:  p.second.setBlue( rev(p.second.blue(),  true));
        break;
        default: break;
        }
    }

    Lux fLux{ p.first, fMir };
    Lux mLux{ p.second, sMir };

    return Tux{ (!fMir ? fLux : mLux), (sMir ? mLux : fLux), siil };
}

Sheet::Sheet(QWidget* parent): QWidget(parent),
    front_{ new QLabel() },
    palette_{}
{
    setLayout(new QGridLayout(this));
    layout()->setContentsMargins(00, 00, 00, 00);
    layout()->addWidget(front_);
}

void Sheet::setSize(QSize size)
{
    setFixedSize(size);

    if (size.isEmpty())
        return;

    front_->setPixmap(front_->pixmap()->scaled(size));
    palette_.setBrush(QPalette::Background, palette_.background().texture().scaled(size));
}

void Sheet::project(QPixmap* front, QPixmap* back)
{
    if (front == nullptr)
    {
        setVisible(false);
        return;
    }

    setVisible(true);
    front_->setPixmap(*front);

    if (back)
    {
        QPixmap background{ back->size() };
        QPainter p{ &background };
        p.setOpacity(0.5f);
        p.drawPixmap(00, 00, *back);

        palette_.setBrush(QPalette::Background, background);
        setPalette(palette_);
    }

    setSize(front->size());
}
