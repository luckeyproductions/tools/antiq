/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DRYWIDGET_H
#define DRYWIDGET_H

#include <QAction>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QSplitter>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QToolBar>

#include "weaver.h"

#include "drywidget.h"

class FileWidget: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(FileWidget, DryWidget)

public:
    explicit FileWidget(Context* context, QWidget *parent = nullptr);

    void setFileButtonsVisible(bool visible);
    virtual bool setOrientation(Qt::Orientation orientation) override;

    QList<QAction*>& fileOperations() { return fileOperations_; }
    QGroupBox* pickerBox() const {      return pickerBox_; }

    void updateTitle();
    const QString& title() const { return title_; }

public slots:
    void updatePickerBoxTitle();

protected slots:
    virtual void actionNew()  {}
    virtual void actionOpen() {}
    virtual void actionSave() {}

protected:
    void createPickerBox();
    virtual void createToolBar();

    void resizeEvent(QResizeEvent* event) override;

    String fileName_;
    QList<QAction*> fileOperations_;
    QToolBar* fileToolBar_;
    QToolBar* actionsToolBar_;
    QWidget* toolBarSpacer_;
    QHBoxLayout* toolBarLayout_;
    QGroupBox* pickerBox_;
    bool firstShow_;

private:
    bool tooLong(QString title);

    void setTitle(const QString& title);
    QString title_;
};

class FlipSplitter : public QSplitter
{
    Q_OBJECT

public:
    FlipSplitter(QWidget* parent = nullptr, int snap = -1);
    void setStretchFactors(Qt::Orientation orientation, int first, int second);
    void setThreshold(double threshold) { threshold_ = threshold; }

signals:
    void orientationChanged(Qt::Orientation);

public slots:
    void resizeEvent(QResizeEvent* event) override;

private:
    std::map<Qt::Orientation, std::pair<int, int>> stretchFactors_;
    float threshold_;
    const int squareSnap_;

private slots:
    void snap();
};

#endif // DRYWIDGET_H
