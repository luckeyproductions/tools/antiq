#ifndef SNAIL_H
#define SNAIL_H

#include <QPushButton>
#include <QLabel>

#include "lumwidget.h"

class Hololith;

class SwitchWidget: public QWidget
{
    Q_OBJECT

public:
    SwitchWidget(QWidget* parent = nullptr);
protected:
    void resizeEvent(QResizeEvent* e);

private slots:
    void switchSnail();

private:
    void updateSnails();

    Channel active_;
    QLabel* primary_;
    std::pair<QPushButton*, QPushButton*> reserve_;
};

class Snail: public LumWidget
{
    Q_OBJECT

public:
    explicit Snail(QWidget* parent = nullptr);

    void feed(unsigned char c);

protected:
    void resizeEvent(QResizeEvent* e) override;
    int gridInt() const override { return lumArea_->height() / 06; }
    void updateGrid() override;

private:
    SwitchWidget* switchWidget_;
};

#endif // SNAIL_H

// Items For Red and Blue

/// Coin: 0^|^a
//
/// Cheesewedge: Transition
//
/// Screw: Operator [ + - * ]
/// Unlocks operator bitsets

// Items for Green
/// Scroll: Operator [ / ] put Nth cypher from scroll
///  "/_" Rip scroll

/// Snails

/// Red
// Fat Communist Construction Wordsmith

/// Blue
// Bottom of the ocean "blue dragon"

/// Green
// Galand beach chaosmagic neophyte gardener

///
