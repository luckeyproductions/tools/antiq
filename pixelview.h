#ifndef PIXELVIEW_H
#define PIXELVIEW_H

#include "holoview.h"

class PixelView: public HoloView
{
    Q_OBJECT
public:
    PixelView(QWidget* parent = nullptr);
};

#endif // PIXELVIEW_H
