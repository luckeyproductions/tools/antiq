#include "qocoon.h"
#include "specter.h"

#include "laser.h"

Viser::Viser(Laser* parent): QLabel(parent),
    laser_{ parent },
    viewWidth_{ 023 },
    viewHeight_{ 05 },
    view_{ viewWidth_ * CELL, viewHeight_ * CELL },
    background_{ viewWidth_ * CELL, viewHeight_ * CELL }
{
}

void Viser::repaintBackground()
{
    QPainter ground{ &background_ };
    QPixmap grid{ LumArea::grid(CELL) };

    const QSize plasmeSize{ laser_->visibleSize() };

    if (plasmeSize.isEmpty())
    {
        ground.setBrush(Qt::black);
        ground.drawRect(QRect{ QPoint{ 00, 00},
                               QSize{ CELL * viewWidth_,
                                      CELL * viewHeight_ } });
    }

    for (int x{ 00 }; x < viewWidth_; ++x)
        for (int y{ 00 }; y < viewWidth_; ++y)
        {
            QPoint cell{ QPoint{ x, y } };
            QPoint loc{ origin() + laser_->scout() + cell };
            QPoint pos{ cell * CELL };

            if (loc.x() < 00 || loc.y() < 00
             || loc.x() >= plasmeSize.width()
             || loc.y() >= plasmeSize.height())
            {
                ground.setBrush(Qt::black);
                ground.drawRect(QRect{ pos, QSize{ CELL, CELL } });
            }
            else
            {
                ground.drawPixmap(pos, grid);
            }
        }

    ground.end();
}

void Viser::updateView()
{
    QPainter lens{ &view_ };
    lens.drawPixmap( 00, 00, background_);

    const QSize plasmeSize{ laser_->visibleSize() };

    if (!plasmeSize.isEmpty())
    {   for (int x{ 00 }; x < viewWidth_; ++x)
        for (int y{ 00 }; y < viewWidth_; ++y)
            {
                QPoint cell{ QPoint{ x, y } };
                QPoint scout{ laser_->scout() };
                QPoint loc{ origin() + scout + cell };
                QPoint pos{ CELL * cell  };
                QRect cellRect{ pos, CELL * QSize{ 01, 01 } };


                QChar c{ Qocoon::specter_->charAt(loc, RED) };
                lens.setPen(Qt::red);
                lens.drawText(cellRect, Qt::AlignCenter, c);

                if (loc == scout)
                    lens.drawRect(cellRect);
            }
    }

    lens.end();
    setPixmap(view_);
}

Laser::Laser(QWidget* parent): QToolBar(parent),
    scout_{ 00, 00 },
    aim_{ East },
    viser_{ new Viser{ this } }
{
    addWidget(viser_);
}

void Laser::target(Plasme* p)
{
    scout_ = QPoint{ 00, 00 };
    aim_ = East;

    viser_->repaintBackground();
    viser_->updateView();
}

void Laser::target(QPoint p, Direction aim, bool mir)
{
    if (scout_ != p)
    {
        scout_ = p;
        viser_->repaintBackground();
    }

    aim_ = aim;
    viser_->updateView();
}

QSize Laser::visibleSize() const
{
    return Qocoon::specter_->plasme()
            ? Qocoon::specter_->plasme()->size()
            : QSize{ 00, 00 };
}

void Laser::burn(unsigned char c, Channel d, bool walk)
{
    if (d == FULL)
        return;


    if (c == '{' || c == '[')
    {
        aim_ = static_cast<Direction>(aim_ + 02);
        return_.push_back(QPoint{ scout_.x() + 01, 00 });
    }
    else if (c == '}' || c == ']')
    {
        target( return_.back(), QPit::direction(d));
        return_.pop_back();

        return;
    }
    else if (c == '<')
    {
        return_.push_back(QPoint{ scout_.x(), scout_.y() + 01 });

        turn(-01);
    }
    else if (c == '>')
    {
        target( return_.back(), South);
        return_.pop_back();

        return;
    }

    QPoint cell{ scout_ };

    if (walk)
        target(scout_ + QPit::direction(aim_), aim_);

    emit fire(cell, c, d);
}

void Scout::step()
{
    if (aim_ == Pi)
    {
        return; //Deathclock
    }
    else if (aim_ == Base)
    {
        pos_ = QPoint{ 00, 00 };
        aim_ = QPit::direction(RED);
        return;
    }

    QPoint& to{ pos_ };

    to += QPit::direction(aim_);

    QSize plasmeSize{ Qocoon::specter_->plasme()->size() };
    to.rx() %= plasmeSize.width();
    to.ry() %= plasmeSize.height();
}
