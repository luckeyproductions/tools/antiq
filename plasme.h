#ifndef PLASME_H
#define PLASME_H

#include "qpdefs.h"

#include <QPixmap>

class Plasme: public std::pair<QPixmap, QPixmap>
{
    Q_GADGET
public:
    Plasme(unsigned char p = 00);

    void clear();

    QSize size() { return size(mach_); }
    Plasme fundum(); // Returns the original of the capo

private:
    void dust();

    static QSize size(unsigned p) { return QSize{ 01, 01 } * mach(p); }
    static unsigned mach(unsigned char p) { return 01u << (03u * p); }

    unsigned char mach_;
};


#endif // PLASME_H
