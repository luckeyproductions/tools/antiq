/* Antiq
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDebug>

#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QUrl>
#include <QAction>
#include <QResizeEvent>
#include <QVBoxLayout>

#include "hololith.h"
#include "flux.h"
#include "specter.h"
#include "Armer/armer.h"
#include "snail.h"
#include "weaver.h"
#include "project.h"
#include "qocoon.h"

auto EMPTY_SCENE = []()
{
    Scene* scene{ new Scene(Qocoon::project()->GetContext()) };
    scene->CreateComponent<Octree>();
    return scene;
};

Flux*    Qocoon::flux_   { nullptr };
Specter* Qocoon::specter_{ nullptr };
Armer*   Qocoon::armer_  { nullptr };
Snail*   Qocoon::snail_  { nullptr };
SharedPtr<Project> Qocoon::project_{ nullptr };

Qocoon::Qocoon(Context* context): QMainWindow(), Object(context),
    singleWidget_{ false },
    mode_{ ViewALL },
    modeActions_{},
    projectDependentActions_{},
    mainToolBar_{ nullptr },
    hololith_{ new Hololith{ context, this } },
    hiddenDocks_{},
    undoView_{ new QUndoView{ this } },
    fullViewAction_{ new QAction{QIcon{ ":/FullView" }, "Toggle Lum", this} },
    recentMenu_{ nullptr }
{

    flux_    = new Flux(this);
    specter_ = new Specter(this);
    armer_   = new Armer(this);
    snail_   = new Snail(this);

    context_->RegisterSubsystem(this);
    setWindowIcon(QIcon{ ":/Icon" });
    setCentralWidget(hololith_);
    setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);

    undoView_->setEmptyLabel("Initial");
    QDockWidget* undoDockWidget{ new QDockWidget{ "Undo History", this } };
    undoDockWidget->setObjectName("Undo History");
    undoDockWidget->setWidget(undoView_);
    addDockWidget(Qt::LeftDockWidgetArea, undoDockWidget);

    tabifyDockWidget(undoDockWidget,
                     createDockWidget(flux_, Qt::LeftDockWidgetArea));
    createDockWidget(specter_, Qt::LeftDockWidgetArea);
    createDockWidget(armer_, Qt::BottomDockWidgetArea);
    createDockWidget(snail_, Qt::BottomDockWidgetArea);

    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomLeftCorner,  Qt::BottomDockWidgetArea);


    fullViewAction_->setCheckable(true);
    fullViewAction_->setShortcut(QKeySequence{ "F11" });
    addAction(fullViewAction_);
    connect(fullViewAction_, SIGNAL(triggered(bool)), this, SLOT(toggleFullView(bool)));

    connect(this, SIGNAL(currentProjectChanged()), SLOT(updateAvailableActions()));
    ///////!
    //    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
    //            view3D_, SLOT(updateView()));
    //    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
    //            view3D_, SLOT(updateView()));
    ///////^

    createToolBar();
    createMenuBar();
    setStatusBar(new QStatusBar{ this });
    loadSettings();
    updateRecentProjects();

    show();
    specter_->reveal(new Plasme(02));

    connect(armer_, SIGNAL(hammer(Mes)), specter_, SLOT(putMes(Mes)));


    ///Ikalo: Musical sidekick
}
/*
Qocoon::Qocoon(Context* context, Material* material): QMainWindow(), Object(context),
    singleWidget_{ true },
    mode_{ EM_AllModes }
{
    context_->RegisterSubsystem(this);

    setWindowIcon(QIcon{ ":/Icon" });
    resize(sizeHint().width() * 3, sizeHint().height() * 7);
    show();

    QMenu* fileMenu{ createMenuBar() };

    QToolBar* toolBar{ new QToolBar{ "Toolbar" } };
    addToolBar(toolBar);
}
*/
Qocoon::~Qocoon()
{
    if (singleWidget_)
        return;

    QSettings settings{};

    if (fullViewAction_->isChecked())
        fullViewAction_->trigger();

    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
}

void Qocoon::loadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
}
void Qocoon::createToolBar()
{
    mainToolBar_ = new QToolBar("Toolbar");
    mainToolBar_->setObjectName("MainToolbar");

    for (ViewMode mode: MODES)
    {
        QString modeName{ modeToString(mode) };
        QAction* action{ new QAction(modeName + " View", this) };
        action->setProperty("mode", mode);
        action->setIcon(QIcon(":/" + modeName + "View" ));
        action->setCheckable(true);
        modeActions_[mode] = action;
        action->setShortcut(QKeySequence("Ctrl+Shift+" + QString::number(mode)));

        connect(action, SIGNAL(triggered(bool)), this, SLOT(changeMode(bool)));
        mainToolBar_->addAction(action);
    }

    mainToolBar_->addSeparator();

    for (bool undo: { true, false })
    {
        QString  actionName{ QString(undo ? "Un" : "Re") + "do" };
        QAction* action{ new QAction(actionName, this) };
        action->setObjectName(actionName + "action");
        action->setIcon(QIcon(":/" + actionName ));
        action->setShortcut(QKeySequence("Ctrl+" + QString(undo ? "" : "Shift+") + "Z"));

        if (undo)
            connect(action, SIGNAL(triggered(bool)), this, SLOT(undo()));
        else
            connect(action, SIGNAL(triggered(bool)), this, SLOT(redo()));

        mainToolBar_->addAction(action);
    }

    addToolBar(mainToolBar_);
    setMode(ViewOFF);
}

QMenu* Qocoon::createMenuBar()
{
    Weaver* weaver{ GetSubsystem<Weaver>() };

    QMenu* fileMenu{ new QMenu("File") };

    fileMenu->addAction(QIcon(":/New"), "New Project...", this, SLOT(newPlasme()), QKeySequence("Ctrl+N"));
    fileMenu->addAction(QIcon(":/Open"), "Open Project...", this, SLOT(openProject()), QKeySequence("Ctrl+O"));
    recentMenu_ = fileMenu->addMenu("Recent Projects");
    connect(this, SIGNAL(recentProjectsChanged()), SLOT(updateRecentProjectMenu()));
    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(), "Close Project...",
                                                           this, SLOT(closeProject())));
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(":/Save"), "Save Project...",
                                                           this, SLOT(saveProject()), QKeySequence("Ctrl+S")));
    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(":/Open"), "Add Resource Folder...",
                                                           this, SLOT(addResourceFolder())));
    fileMenu->addSeparator();
    fileMenu->addAction(QIcon(":/Quit"), "Exit", weaver, SLOT(quit()), QKeySequence("Ctrl+Q"));
    menuBar()->addMenu(fileMenu);

    if (!singleWidget_)
    {
        QMenu* viewMenu{ new QMenu("View") };
        viewMenu->addAction(fullViewAction_);
        menuBar()->addMenu(viewMenu);
    }

    QMenu* helpMenu{ new QMenu("Help") };
    helpMenu->addAction(QIcon(":/About"), tr("About %1...").arg(Weaver::applicationDisplayName()), this, SLOT(about()));
    menuBar()->addMenu(helpMenu);

    updateAvailableActions();

    return fileMenu;
}

void Qocoon::updateAvailableActions()
{
    for (QAction* action: projectDependentActions_)
        action->setEnabled(project() != nullptr);
}

void Qocoon::updateRecentProjectMenu()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};

    recentMenu_->clear();

    for (const QString& recent: recentProjects)
    {
        QString displayName{ recent.left(recent.length() - (String(FILENAME_PROJECT).Length() + 1)) };
        recentMenu_->addAction(displayName, this, SLOT(openRecent()))->setObjectName(recent);
    }
}

void Qocoon::updateRecentProjects()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};
    QString currentProject{ toQString(projectLocation() + FILENAME_PROJECT) };

    for (const QString& recent: recentProjects)
    {
        if (!QFileInfo(recent).exists() || currentProject == recent )
            recentProjects.removeOne(recent);
    }

    if (project_)
        recentProjects.push_front(currentProject);

    while (recentProjects.size() > 10)
        recentProjects.pop_back();

    settings.setValue("recentprojects", recentProjects);

    emit recentProjectsChanged();
}


void Qocoon::newPlasme()
{
    QString startPath{ project_ ? toQString(GetParentPath(projectLocation()))
                                : QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };

    String selectedFolder{ toString(QFileDialog::getExistingDirectory(nullptr, tr("Create Project"), startPath)) };

    if (selectedFolder.IsEmpty()) // Cancelled
        return;
    else
        selectedFolder = AddTrailingSlash(selectedFolder);

    String projectFileName{ selectedFolder + FILENAME_PROJECT };

    if (QFileInfo(toQString(projectFileName)).exists())
    {
        statusBar()->showMessage("A project already exists in this folder");
        return;
    }
    else
    {
        SharedPtr<Project> newProject{ new Project(context_) };
        newProject->setName(selectedFolder.Split('/').Back());
        newProject->setLocation(selectedFolder);

        if (!newProject->Save() ||
            !loadProject(toQString(projectFileName)))
        {
            statusBar()->showMessage("Failed to create new project");
        }
    }
}
/* Add bookmark    ?
    if (QFileInfo(toQString(projectFileName)).exists())
    {
        statusBar()->showMessage("A project already exists in this folder");
        return;
    }
    else
    {
        SharedPtr<Project> newProject{ new Project(context_) };
        newProject->setName(selectedFolder.Split('/').Back());
        newProject->setLocation(selectedFolder);

        if (!newProject->Save() ||
            !loadProject(toQString(projectFileName)))
        {
            statusBar()->showMessage("Failed to create new project");
        }
    }
*/
void Qocoon::openProject(QString filename)
{
    if (filename.isEmpty()) // Pick a project through a file dialog
    {
        QString startPath{ project_ ? toQString(projectLocation())
                                    : QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };

        String selectedProject{ toString(QFileDialog::getOpenFileName(nullptr, tr("Open Project"), startPath, "*.XML")) };
        if (selectedProject.IsEmpty()) // Cancelled
            return;

        filename = toQString(selectedProject);
    }

    loadProject(filename);
}

bool Qocoon::loadProject(QString filename)
{
    const String path{ AddTrailingSlash(GetPath(toString(filename))) };

    if (project_ && project_->location() == path)
    {
        statusBar()->showMessage("Project already open");
        return false;
    }

    if (QFileInfo(filename).exists())
    {
        SharedPtr<XMLFile> projectFile{ GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(toString(filename)) };

        if (!projectFile.IsNull() && !projectFile->GetRoot("project").IsNull())
        {
            SharedPtr<Project> newProject{ new Project(context_) };

            newProject->setLocation(path);

            if (!newProject->LoadXML(projectFile->GetRoot("project")))
                goto failed;

            project_ = newProject;
//            setScene(EMPTY_SCENE());
            hololith_->setCurrentIndex(1);

            setWindowTitle(toQString(project()->name()));
            statusBar()->showMessage("Opened project " + toQString(project_->name()));
            updateRecentProjects();

            emit currentProjectChanged();
            return true;
        }
    }

    failed:
    statusBar()->showMessage("Failed to open project");
    return false;
}

bool Qocoon::saveProject()
{
    if (project_)
        return project_->Save();
    else
        return false;
}

void Qocoon::closeProject() //\\\ Should check for modified files
{
    if (!project_)
        return;

    hololith_->setCurrentIndex(0);

    project_->remove();
    project_ = nullptr;

    emit currentProjectChanged();
}

void Qocoon::addResourceFolder()
{
    String selectedFolder{ toString(QFileDialog::getExistingDirectory(
                                        nullptr, tr("Add Resource Folder"),
                                        toQString(projectLocation()))) };

    if (selectedFolder.IsEmpty()) // Cancelled
        return;

    statusBar()->showMessage("Added resource folder " + toQString(selectedFolder));
    project_->addResourceFolder(selectedFolder);
}

String Qocoon::trimmedResourceName(const String& fileName)
{
    if (!project_->inResourceFolder(fileName))
        return "";
    else
        return fileName.Replaced(containingFolder(fileName), "");
}

String Qocoon::containingFolder(const String& path)
{
    if (project_) for (String folder: project_->resourceFolders())
    {
        if (path.Contains(project()->absoluteResourceFolder(folder)))
            return folder;
    }

    return "";
}

String Qocoon::locateResourceRoot(String resourcePath)
{
    if (resourcePath.IsEmpty())
        return "";


    if (project_ && project()->inResourceFolder(resourcePath))
        return containingFolder(resourcePath);

    String trail{ resourcePath };

    while (GetParentPath(trail) != trail)
    {
        String projectFile{ trail + FILENAME_PROJECT };

        if (QFileInfo(toQString(projectFile)).exists())
            return trail;

        trail = GetParentPath(trail);
    }

    return "";
}

String Qocoon::projectLocation()
{
    if (project_)
        return GetPath(project_->location());
    else
        return "";
}

void Qocoon::toggleFullView(bool toggled)
{
    QSettings settings{};

    if (toggled) {

        settings.setValue("geometry", saveGeometry());
        settings.setValue("state", saveState());

        for (QDockWidget* dock: findChildren<QDockWidget*>()) {
            if (dock->isVisible()) {
                dock->setVisible(false);
            }
        }

        statusBar()->setVisible(false);
        menuBar()->setVisible(false);
        mainToolBar_->setVisible(false);

        if (!isFullScreen())
            showFullScreen();

        fullViewAction_->setShortcuts({ QKeySequence("F11"), QKeySequence("Esc") });

    } else {


        statusBar()->setVisible(true);
        menuBar()->setVisible(true);

        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("state").toByteArray());

        if (isFullScreen())
            showMaximized();

        fullViewAction_->setShortcuts({ QKeySequence("F11") });
    }
}

void Qocoon::resizeEvent(QResizeEvent* event)
{
    if (static_cast<double>(event->size().width()) / event->size().height() > 1.0) {

        setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);


    } else {

        setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
    }
}

void Qocoon::setMode(ViewMode mode)
{
    if (mode == mode_ || mode >= ViewALL )
        mode_ = ViewOFF;
    else
        mode_ = mode;

    for (ViewMode m: modeActions_.keys())
    {
        modeActions_[m]->setChecked(m == mode_);
    }

    hololith_->setCurrentIndex(mode_);
}
void Qocoon::changeMode(bool toggled)
{
    setMode(static_cast<ViewMode>(sender()->property("mode").toInt()));
}
QString Qocoon::modeToString(ViewMode mode)
{
    switch (mode)
    {
    case ViewONE: return "Pixel"; break;
    case ViewTWO:    return "2D"; break;
    case ViewTHREE:  return "3D"; break;
    default:          return ""; break;
    }
}

void Qocoon::about()
{
    QString aboutText{ tr("<p>Copyleft 🄯 2020 <a href=\"https://luckeyproductions.nl\">LucKey Productions</a></b>"
                          "<p>You may use and redistribute this software under the terms "
                          "of the<br><a href=\"https://www.gnu.org/licenses/gpl.html\">"
                          "GNU General Public License Version 3</a>.</p>") };

    QDialog* aboutBox{ new QDialog(this) };

    aboutBox->setWindowTitle("About " + Weaver::applicationDisplayName());
    QVBoxLayout* aboutLayout{ new QVBoxLayout() };
    aboutLayout->setContentsMargins(0, 8, 0, 4);

    QPushButton* antiqButton{ new QPushButton() };
    QPixmap antiqLogo{ ":/Logo" };
    antiqButton->setIcon(antiqLogo);
    antiqButton->setFlat(true);
    antiqButton->setMinimumSize(antiqLogo.width() * 04, antiqLogo.height());
    antiqButton->setIconSize(antiqLogo.size());
    antiqButton->setToolTip("https://gitlab.com/luckeyproductions/hantik");
    antiqButton->setCursor(Qt::CursorShape::PointingHandCursor);
    aboutLayout->addWidget(antiqButton);
    aboutLayout->setAlignment(antiqButton, Qt::AlignHCenter);
    connect(antiqButton, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    QLabel* aboutLabel{ new QLabel(aboutText) };
    aboutLabel->setWordWrap(true);
    aboutLabel->setAlignment(Qt::AlignJustify);
    QVBoxLayout* labelLayout{ new QVBoxLayout() };
    labelLayout->setContentsMargins(42, 34, 42, 12);
    labelLayout->addWidget(aboutLabel);
    aboutLayout->addLayout(labelLayout);

    QDialogButtonBox* buttonBox{ new QDialogButtonBox(QDialogButtonBox::Ok, aboutBox) };
    connect(buttonBox, SIGNAL(accepted()), aboutBox, SLOT(accept()));
    aboutLayout->addWidget(buttonBox);

    aboutBox->setLayout(aboutLayout);
    aboutBox->resize(aboutBox->minimumSize());
    aboutBox->exec();
}
void Qocoon::openUrl()
{
    QDesktopServices::openUrl(QUrl(qobject_cast<QPushButton*>(sender())->toolTip()));
}

void Qocoon::undo()
{/*
    if (view3D_->scene()) {

        sceneStack(view3D_->scene())->undo();
    }
*/}
void Qocoon::redo()
{/*
    if (view3D_->scene()) {

        sceneStack(view3D_->scene())->redo();
    }
*/}
