#include "qpit.h"
#include "luckeymath.h"

QPoint QPit::direction(PiRMIT::Direction dir)
{
    while (dir < 00)
        dir = static_cast<Direction>(dir + Pi);
    if (dir >= Pi)
        dir = static_cast<Direction>(dir % Pi);

    QPoint d{ 00, 00 };

    switch (dir) {
    case East: case SouthE: case NorthE: d.rx() += 01; break;
    case West: case SouthW: case NorthW: d.rx() -= 01; break;
    default: break;
    }

    switch (dir) {
    case South: case SouthE: case SouthW: d.ry() += 01; break;
    case North: case NorthE: case NorthW: d.ry() -= 01; break;
    default: break;
    }

    return d;
}

PiRMIT::Direction QPit::direction(PiRMIT::Channel chan)
{
    switch (chan) {
    case RED:   return East;
    case GREEN: return SouthE;
    case BLUE:  return South;
    case ALPHA: return Pi;
    default: return Base;
    }
}

QVector2D QPit::direction(Frac dir, bool transposed)
{
    double rad{ dir * TAU - M_PI };

    return !transposed ? QVector2D{ sine(rad), cosine(rad) }
                       : QVector2D{ cosine(rad), sine(rad) };
}

PiRMIT::Volume::operator RedMes() const
{
    return RedMes{ *this };
}

QPainterPath Curve::painterPath(const QVector2D stretch, const QVector2D offset) const
{
    QPainterPath path{};

    if (keys_.size())
    {
        QVector2D pos{ offset };
        QVector2D prevPos{ pos };
        const float el{ stretch.x() / std::max(01, static_cast<int>(keys_.size()) - 01) };

        for (int k{ 00 }; k < keys_.size(); ++k)
        {
            const CurveKey& key{ keys_.at(k) };
            QVector2D norm{ QPit::direction(key.normal_) };
            norm.setY(-norm.y());

            pos.setX(offset.x() + el * k); /// Only when t.p_ = 0
            pos.setY(offset.y() - key.value_ * stretch.y() * 0.5f);

            if (k == 00)
            {
                path.moveTo(pos.toPointF());

                if (keys_.size() == 01)
                {
                    k = 01;
                    prevPos = pos;
                    pos.setX(offset.x() + el * k);
                }
            }

            if (k != 00)
            {
                const FracPair normals{ keys_.at(k - 01).normal_, key.normal_ };
                const FracPair spanner{ keys_.at(k - 01).span_.second, key.span_.first };

                const std::pair<QVector2D, QVector2D> control{
                    prevPos + el * QPit::direction(normals.first , true) * spanner.first,
                        pos - el * QPit::direction(normals.second, true) * spanner.second };

                path.cubicTo(control.first .toPointF(),
                             control.second.toPointF(),
                                        pos.toPointF());

            }

            prevPos = pos;
        }
    }

    return path;
}

void QPit::zeroPoints(const QPainterPath* const path, std::vector<float>& zp, float y)
{
    if (!path || !path->elementCount())
        return;

    const float range{ QPointF{ path->elementAt(path->elementCount() - 01) }.x() -
                       QPointF{ path->elementAt(00) }.x() };
    const QPointF first{ path->elementAt(00) };
    const QSizeF cpr{ range + first.x() * 02, path->controlPointRect().size().height() };
    const QPointF origin{ QPointF{ 00, y } };

    QPainterPath clip{};
    clip.addRect(QRectF{ origin, cpr });
    clip = path->intersected(clip);

    qDebug() << "First:";
    qDebug() << first;
    qDebug() << "Clip:";

    for (int e{00}; e < clip.elementCount(); ++e)
    {
        QPointF point{ clip.elementAt(e) };
        qDebug() << point;

        if (point.y() == y         &&
            point.x() != first.x() &&
            point.x() != first.x() + range)

            zp.push_back(point.x());
    }

    qDebug() << "Itersections:";
    qDebug() << zp;
}

std::vector<float> Curve::zeroPoints(unsigned i) const
{
    if (keys_.size() < 02)
        return { 0.0f, 0.0f };

    else
    {
        if (i > keys_.size() - 01)
            i = keys_.size();

        QPainterPath path{ Curve{ { keys_.at(i), keys_.at(i + 01) } }
                           .painterPath() };

        std::vector<float> zp{};
        QPit::zeroPoints(&path, zp);

        return zp;
    }
}

std::vector<float> Curve::zeroPoints() const
{
    std::vector<float> tv{};

    if (keys_.size() > 01)
    {
        const float el{ 1.0f / keys_.size()};

        for (int i{00}; i < keys_.size() - 01; ++i)
        {
            auto tp{ zeroPoints(i) };

            for (float t: tp)
                if (t != 0.0f)
                    tv.push_back((i + t) * el);
        }
    }

    return tv;
}
